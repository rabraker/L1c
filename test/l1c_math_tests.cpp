#include <cjson/cJSON.h>
#include <gtest/gtest.h>
#include <math.h> //Constants
#include <stdlib.h>

#include "l1c.h"
#include "l1c_math.h"

#include "common.h"

namespace l1c {

TEST(L1cMath, L1cInitVec) {
  int N = 6;
  double a = 3.5;
  double x[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0};

  l1c_init_vec(N, x, a);

  for (int i = 0; i < N; i++) {
    ASSERT_NEAR(a, x[i], kDoubleTolSuper);
  }
}

TEST(L1cMath, L1cDaxpyZ2) {
  int N = 256 * 256;

  double* xx = l1c_malloc_double(2 * N);
  double* yy = l1c_malloc_double(2 * N);
  double* zz_exp = l1c_malloc_double(2 * N);
  double* zz = l1c_malloc_double(2 * N);

  double alp = 4.5918;
  for (int i = 0; i < 2 * N; i++) {
    xx[i] = ((double)rand()) / 256 / 256;
    yy[i] = ((double)rand()) / 256 / 256;
  }

  for (int i = 0; i < 2 * N; i++) {
    zz_exp[i] = alp * xx[i] + yy[i];
  }

  l1c_daxpy_z(N, alp, xx, yy, zz);
  l1c_daxpy_z(N, alp, xx + N, yy + N, zz + N);

  ASSERT_CVEC_NEAR(N, zz_exp, zz, kDoubleTolSuper);

  l1c_free_double(xx);
  l1c_free_double(yy);
  l1c_free_double(zz_exp);
  l1c_free_double(zz);
}

TEST(L1cMath, l1cDaxpyZ) {
  int N = 6;
  double a = 3;
  double x[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0};
  double y[] = {2.0, 2.0, 2.0, 2.0, 2.0, 2.0};
  double z[] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
  double z_exp[] = {5.0, 8.0, 11.0, 14.0, 17.0, 20.0};

  l1c_daxpy_z(N, a, x, y, z);

  ASSERT_CVEC_NEAR(N, z_exp, z, kDoubleTol);
}

TEST(L1cMath, L1cDaxpbyZz) {
  int N = 6;
  double a = 3;
  double b = 2;

  double x[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0};
  double y[] = {2.0, 2.0, 2.0, 2.0, 2.0, 2.0};
  double z[] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0};

  double z_exp[] = {7.0, 10.0, 13.0, 16.0, 19.0, 22.0};

  l1c_daxpby_z(N, a, x, b, y, z);

  ASSERT_CVEC_NEAR(N, z_exp, z, kDoubleTol);
}

TEST(L1cMath, L1cDxMulyZ) {
  int N = 6;

  double x[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0};
  double y[] = {2.0, 2.0, 2.0, 2.0, 2.0, 2.0};
  double z[] = {0.0, 0.0, 0.0, 0.0, 0.0, .0};
  double z_exp[] = {2.0, 4.0, 6.0, 8.0, 10.0, 12.0};
  l1c_dxmuly_z(N, x, y, z);

  ASSERT_CVEC_NEAR(N, z_exp, z, kDoubleTol);
}

TEST(L1cMath, L1cDsum) {

  int N = 6;
  double x[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0};
  double sum_exp = 21.0;
  double sum_x = l1c_dsum(N, x);

  ASSERT_NEAR(sum_exp, sum_x, kDoubleTol);
}

TEST(L1cMath, L1cDnorm1) {

  int N = 6;
  double x[] = {1.0, 2.0, -3.0, -4.0, 5.0, 6.0};
  double sum_exp = 21.0;
  double sum_x = l1c_dnorm1(N, x);

  ASSERT_NEAR(sum_exp, sum_x, kDoubleTol);
}

TEST(L1cMath, L1cDlogSum) {
  int N = 6;
  double x[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0};
  double alpha = 3.0;
  double logsum_x_exp = 13.170924944018758;
  double logsum_x = l1c_dlogsum(N, alpha, x);

  ASSERT_NEAR(logsum_x_exp, logsum_x, kDoubleTol);
}

TEST(L1cMath, L1cdnrm2Err) {
  int N = 6;
  double x_[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0};
  double y_[] = {0.0, 1.0, 2.0, 3.0, 4.0, 5.0};

  double* x = l1c_malloc_double(N);
  double* y = l1c_malloc_double(N);
  ASSERT_NE(x, nullptr);
  ASSERT_NE(y, nullptr);

  for (int i = 0; i < N; i++) {
    x[i] = x_[i];
    y[i] = y_[i];
  }

  double nrm_x_y = l1c_dnrm2_err(N, x, y);

  ASSERT_NEAR(nrm_x_y, sqrt(N), kDoubleTolSuper);

  l1c_free_double(x);
  l1c_free_double(y);
}

TEST(L1cMath, L1cdnrm2RelEerr) {
  int N = 6;
  double x_[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0};
  double y_[] = {0.0, 1.0, 2.0, 3.0, 4.0, 5.0};

  double* x = l1c_malloc_double(N);
  double* y = l1c_malloc_double(N);
  ASSERT_NE(x, nullptr);
  ASSERT_NE(y, nullptr);

  for (int i = 0; i < N; i++) {
    x[i] = x_[i];
    y[i] = y_[i];
  }

  double nrm_x_y = l1c_dnrm2_rel_err(N, x, y);

  ASSERT_NEAR(nrm_x_y, sqrt(N) / sqrt(55.0), kDoubleTolSuper);

  l1c_free_double(x);
  l1c_free_double(y);
}

TEST(L1cMath, L1cMaxVec) {
  int N = 6;
  double xmax;
  double x_[] = {1.0, -2.0, -3.0, 4.0, 5.0, 6.0};

  double* x = l1c_malloc_double(N);
  ASSERT_NE(x, nullptr);
  for (int i = 0; i < N; i++) {
    x[i] = x_[i];
  }

  xmax = l1c_max_vec(0, x);
  ASSERT_TRUE(isnan(xmax)) << xmax;

  xmax = l1c_max_vec(1, x);
  ASSERT_DOUBLE_EQ(1.0, xmax);

  xmax = l1c_max_vec(N, x);
  ASSERT_DOUBLE_EQ(6.0, xmax);

  l1c_free_double(x);
}

TEST(L1cMath, L1cAbsVec) {
  int N = 6;
  double x_[] = {1.0, -2.0, -3.0, 4.0, 5.0, 6.0};
  double x_exp[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0};

  double* x = l1c_malloc_double(N);
  double* xabs = l1c_malloc_double(N);
  ASSERT_NE(x, nullptr);
  ASSERT_NE(xabs, nullptr);

  for (int i = 0; i < N; i++) {
    x[i] = x_[i];
    xabs[i] = 10;
  }

  l1c_abs_vec(N, x, xabs);

  ASSERT_CVEC_NEAR(N, xabs, x_exp, kDoubleTolSuper);

  l1c_free_double(x);
  l1c_free_double(xabs);
}

TEST(L1cMath, l1c_math_max) {
  double a = 5, b = 6, mx = 0;

  mx = max(a, b);
  ASSERT_DOUBLE_EQ(mx, b);

  mx = max(a, -b);
  ASSERT_DOUBLE_EQ(mx, a);
}

TEST(L1cMath, l1c_math_min) {
  double a = 5, b = 6, mx = 0;

  mx = min(a, b);
  ASSERT_DOUBLE_EQ(mx, a);

  mx = min(a, -b);
  ASSERT_DOUBLE_EQ(mx, -b);
}

TEST(L1cMath, l1c_math_imax) {
  int a = 5, b = 6, mx = 0;

  mx = imax(a, b);
  ASSERT_EQ(mx, b);

  mx = imax(a, -b);
  ASSERT_EQ(mx, a);
}

TEST(L1cMath, l1c_math_imin) {
  int a = 5, b = 6, mx = 0;

  mx = imin(a, b);
  ASSERT_DOUBLE_EQ(mx, a);

  mx = imin(a, -b);
  ASSERT_DOUBLE_EQ(mx, -b);
}

} // namespace l1c
