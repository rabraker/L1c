/*
 * Tests for the Bregman Splitting optimizations.
 */
#include <cblas.h>
#include <cjson/cJSON.h>
#include <gtest/gtest.h>
#include <stdio.h>
#include <stdlib.h>

#include "bregman.h"
#include "common.h"
#include "json_utils.h"
#include "l1c.h"
#include "l1c_math.h"
#include "test_config.h"

namespace l1c {

class BregmanFixture : public ::testing::Test {
public:
  void SetUp() override {
    const std::string fpath = std::string(kTestDataDir) + "/bregman.json";
    cJSON* td_json;
    int tmp = 0;
    ASSERT_EQ(0, load_file_to_json(fpath.c_str(), &td_json)) << fpath;

    ASSERT_EQ(0, extract_json_double_array(td_json, "x", &x, &NM));
    ASSERT_EQ(0, extract_json_double_array(td_json, "y", &y, &tmp));
    ASSERT_EQ(0, extract_json_double_array(td_json, "z", &z, &tmp));
    ASSERT_EQ(0, extract_json_double_array(td_json, "x_shrunk", &x_shrunk_exp, &tmp));
    ASSERT_EQ(0, extract_json_double_array(td_json, "dx", &dx, &tmp));
    ASSERT_EQ(0, extract_json_double_array(td_json, "dy", &dy, &tmp));
    ASSERT_EQ(0, extract_json_double_array(td_json, "bx", &bx, &tmp));
    ASSERT_EQ(0, extract_json_double_array(td_json, "by", &by, &tmp));
    ASSERT_EQ(0, extract_json_double_array(td_json, "f", &f, &tmp));
    ASSERT_EQ(0, extract_json_double_array(td_json, "rhs", &rhs_exp, &tmp));
    ASSERT_EQ(0, extract_json_double_array(td_json, "b", &b, &tmp));
    ASSERT_EQ(0, extract_json_double_array(td_json, "Hsolveb", &Hsolveb, &tmp));
    ASSERT_EQ(0, extract_json_double_array(td_json, "Hessx", &Hessx, &tmp));
    ASSERT_EQ(0, extract_json_double_array(td_json, "H_diag", &H_diag_exp, &tmp));

    ASSERT_EQ(0, extract_json_double(td_json, "gamma", &gamma));
    ASSERT_EQ(0, extract_json_double(td_json, "mu", &mu));
    ASSERT_EQ(0, extract_json_double(td_json, "lam", &lam));
    ASSERT_EQ(0, extract_json_int(td_json, "m", &m));
    ASSERT_EQ(0, extract_json_int(td_json, "n", &n));

    cJSON_Delete(td_json);
  }

  void TearDown() override {
    l1c_free_double(x);
    l1c_free_double(y);
    l1c_free_double(z);
    l1c_free_double(x_shrunk_exp);
    l1c_free_double(dx);
    l1c_free_double(dy);
    l1c_free_double(bx);
    l1c_free_double(by);
    l1c_free_double(f);
    l1c_free_double(rhs_exp);
    l1c_free_double(b);
    l1c_free_double(Hsolveb);
    l1c_free_double(Hessx);
    l1c_free_double(H_diag_exp);
  }

  int NM;
  int n;
  int m;
  double* x;
  double* y;
  double* z;
  double* x_shrunk_exp;
  double* dx;
  double* dy;
  double* bx;
  double* by;
  double* f;
  double* rhs_exp;
  double* b;
  double* Hsolveb;
  double* Hessx;
  double* H_diag_exp;
  double gamma;
  double mu;
  double lam;
};

TEST_F(BregmanFixture, test_breg_anis_jacobi) {
  BregFuncs bfuncs = breg_get_functions();

  double* uk = l1c_malloc_double(NM);
  double* dwork = l1c_malloc_double(NM);
  double* D = l1c_malloc_double(NM);
  int i = 0;

  // Must initialize uk, becuase it used recursively in breg_anis_jacobi.
  for (i = 0; i < NM; i++) {
    uk[i] = b[i];
  }
  bfuncs.hess_inv_diag(n, m, mu, lam, D);

  for (i = 0; i < (NM); i++) {
    bfuncs.breg_anis_jacobi(n, m, uk, dwork, b, D, lam);
  }

  ASSERT_CVEC_NEAR(NM, Hsolveb, uk, kDoubleTol);
  l1c_free_double(uk);
  l1c_free_double(dwork);
  l1c_free_double(D);
}

TEST_F(BregmanFixture, test_breg_anis_guass_seidel) {
  BregFuncs bfuncs = breg_get_functions();

  double* uk = l1c_malloc_double(NM);
  cblas_dcopy(NM, b, 1, uk, 1);

  for (int i = 0; i < (NM); i++) {
    bfuncs.breg_anis_guass_seidel(n, m, uk, b, mu, lam);
  }

  ASSERT_CVEC_NEAR(NM, Hsolveb, uk, kDoubleTol);
  l1c_free_double(uk);
}

TEST_F(BregmanFixture, test_breg_shrink1) {

  BregFuncs bfuncs = breg_get_functions();

  double* x_shrunk = NULL;
  int N = 0;

  N = NM;
  x_shrunk = l1c_malloc_double(N);
  ASSERT_NE(nullptr, x_shrunk);
  for (int i = 0; i < N; i++) {
    x_shrunk[i] = 0;
  }

  bfuncs.breg_shrink1(N, x, x_shrunk, gamma);

  ASSERT_CVEC_NEAR(N, x_shrunk_exp, x_shrunk, kDoubleTolSuper * 10);

  l1c_free_double(x_shrunk);
}

TEST_F(BregmanFixture, test_breg_rhs) {
  BregFuncs bfuncs = breg_get_functions();
  double *dwork1, *dwork2, *rhs;

  rhs = l1c_malloc_double(NM);
  dwork1 = l1c_malloc_double(NM);
  dwork2 = l1c_malloc_double(NM);
  ASSERT_NE(nullptr, rhs);
  ASSERT_NE(nullptr, dwork1);
  ASSERT_NE(nullptr, dwork2);
  bfuncs.breg_anis_rhs(n, m, f, dx, bx, dy, by, rhs, mu, lam, dwork1, dwork2);
  ASSERT_CVEC_NEAR(NM, rhs_exp, rhs, kDoubleTolSuper * 10);

  l1c_free_double(rhs);
  l1c_free_double(dwork1);
  l1c_free_double(dwork2);
}

TEST_F(BregmanFixture, test_breg_hess_inv_diag) {
  BregFuncs bfuncs = breg_get_functions();
  double* D;

  D = l1c_malloc_double(NM);
  ASSERT_NE(nullptr, D);

  bfuncs.hess_inv_diag(n, m, mu, lam, D);

  ASSERT_CVEC_NEAR(NM, H_diag_exp, D, kDoubleTolSuper * 10);

  l1c_free_double(D);
}

TEST_F(BregmanFixture, test_breg_mxpy_z) {

  BregFuncs bfuncs = breg_get_functions();
  int N = NM;
  bfuncs.breg_mxpy_z(N, x, y, z);

  ASSERT_CVEC_NEAR(N, z, z, kDoubleTolSuper * 10);
}

/*
  This test doesnt do much (not sure what to check), but
  is here so we can run valgrind on the function.
 */
class BregmanAnis : public ::testing::Test {
public:
  void SetUp() override {
    const std::string fpath = std::string(kTestDataDir) + "/bregman_img.json";
    ASSERT_EQ(0, load_file_to_json(fpath.c_str(), &img_json)) << fpath;

    ASSERT_EQ(0, extract_json_double_array(img_json, "img_vec", &img_vec, &NM));
    ASSERT_EQ(0, extract_json_int(img_json, "n", &n));
    ASSERT_EQ(0, extract_json_int(img_json, "m", &m));
    uk = l1c_malloc_double(n * m);
    ASSERT_NE(uk, nullptr);

    l1c_init_vec(n * m, uk, 0);
  }

  void TearDown() override {
    l1c_free_double(uk);
    l1c_free_double(img_vec);
    cJSON_Delete(img_json);
  }
  cJSON* img_json;

  int n = 0;
  int m = 0;
  int NM = 0;
  double* uk;
  double* img_vec;
  double mu = 5;
  double tol = 0.001;
  int max_iter = 100;
  int max_jac_iter = 1;
};

TEST_F(BregmanAnis, test_breg_anis_TV) {
  int ret = l1c_breg_anistropic_TV(n, m, uk, img_vec, mu, tol, max_iter, max_jac_iter);
  ASSERT_EQ(ret, 0);
}

} // namespace l1c
