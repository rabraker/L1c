/*
This is a test suite for the l1qc_newton library.

 */
#include "cblas.h"
#include <cjson/cJSON.h>
#include <cmath>
#include <gtest/gtest.h>
#include <stdlib.h>

#include "common.h"
#include "json_utils.h"
#include "l1c.h"
#include "l1c_math.h"
#include "nesta.h"
#include "test_config.h"

namespace l1c {

struct NestaGenericData : ::testing::Test {
  void SetUp() override {
    /* !!!!!!!!!!!!!!!! LOAD THIS FROM JSON LATER !!!!!!!!!!!!!!!!!!!*/
    n_continue = 5;
    tol = 1e-3;
    beta_mu = 0;
    beta_tol = 0;

    std::string fpath_generic = std::string(kTestDataDir) + "/nesta_data.json";
    cJSON* json_data;

    l1c_AxFuns ax_funs;

    ASSERT_EQ(0, load_file_to_json(fpath_generic.c_str(), &json_data)) << fpath_generic;
    ASSERT_EQ(0, extract_json_double_array(json_data, "xk", &xk, &m));
    ASSERT_EQ(0, extract_json_double_array(json_data, "b", &b, &n));
    ASSERT_EQ(0, extract_json_double_array(json_data, "g", &g, &m));
    ASSERT_EQ(0, extract_json_double_array(json_data, "yk_exp", &yk_exp, &m));
    ASSERT_EQ(0, extract_json_double_array(json_data, "gradf_exp", &gradf_exp, &m));
    ASSERT_EQ(0, extract_json_int_array(json_data, "pix_idx", &pix_idx, &n));
    ASSERT_EQ(0, extract_json_double(json_data, "fx_exp", &fx_exp));
    ASSERT_EQ(0, extract_json_double(json_data, "mu", &mu));
    ASSERT_EQ(0, extract_json_double(json_data, "L", &L));
    ASSERT_EQ(0, extract_json_double(json_data, "sigma", &sigma));

    BpMode bp_mode = analysis;
    DctMode dct_mode = dct1;
    ASSERT_EQ(0,
              l1c_setup_dctTV_transforms(
                  n, m, 1, 0.0, 0.0, dct_mode, bp_mode, pix_idx, &ax_funs));

    ax_funs.Ax = ax_funs.Rx;
    ax_funs.Aty = ax_funs.Rty;

    NP = _l1c_NestaProb_new(ax_funs);
    ASSERT_NE(NP, nullptr);

    l1c_NestaOpts opts = {
        .mu = mu, .tol = tol, .sigma = sigma, .n_continue = n_continue, .verbose = 0};

    ASSERT_EQ(0, l1c_nesta_setup(NP, &beta_mu, &beta_tol, b, ax_funs, &opts));

    /* Copy the test data into NP*/
    cblas_dcopy(m, xk, 1, NP->xk, 1);
    cblas_dcopy(n, b, 1, NP->b, 1);

    yk = l1c_calloc_double(m);
    ASSERT_NE(yk, nullptr);
  }

  void TearDown() override {
    l1c_free_double(xk);
    l1c_free_double(b);
    l1c_free_double(g);
    l1c_free_double(yk_exp);
    l1c_free_double(gradf_exp);

    free(pix_idx);

    if (NP) {
      NP->ax_funs.destroy();
    }
    l1c_free_nesta_problem(NP);
    l1c_free_double(yk);
  }

  int m;
  int n;

  double mu;
  double sigma;
  double L;
  double tol;
  double fx_exp;

  int* pix_idx;
  double* xk;
  double* b;
  double* g;
  // double *yk;
  double* yk_exp;
  double* gradf_exp;
  double* yk;
  //
  int n_continue;
  double beta_mu;
  double beta_tol;

  l1c_NestaProb* NP;
};

TEST_F(NestaGenericData, test_nesta_project) {
  NP->mu_j = mu;
  l1c_nesta_project(NP, xk, g, yk);

  ASSERT_CVEC_NEAR(m, yk, yk_exp, kDoubleTol);
}

TEST_F(NestaGenericData, NestaFeval) {
  NP->mu_j = NP->mu;
  l1c_nesta_feval(NP);
  ASSERT_CVEC_NEAR(m, gradf_exp, NP->gradf, kDoubleTol);
}

struct L1cNestaOneIterTest : testing::Test {
public:
  void SetUp() override {
    std::string fpath_1iter = std::string(kTestDataDir) + "/lb_test_data_AX.json";
    ASSERT_EQ(0, load_file_to_json(fpath_1iter.c_str(), &json_data)) << fpath_1iter;

    ASSERT_EQ(0, extract_json_double_array(json_data, "x0", &x0, &m));
    ASSERT_EQ(0, extract_json_double_array(json_data, "x_act", &x_exp, &m));
    ASSERT_EQ(0, extract_json_double_array(json_data, "b", &b, &n));

    ASSERT_EQ(0, extract_json_double(json_data, "epsilon", &epsilon));
    ASSERT_EQ(0, extract_json_double(json_data, "enrm1", &enrm1));

    ASSERT_EQ(0, extract_json_int_array(json_data, "T_idx", &T_idx, &T));
    ASSERT_EQ(0, extract_json_int_array(json_data, "TC_idx", &TC_idx, &TC));
    ASSERT_EQ(0, extract_json_double_array(json_data, "A", &A, &mn));

    ASSERT_EQ(mn, n * m);

    tmp1 = l1c_calloc_double(m);
    tmp2 = l1c_calloc_double(m);
    ASSERT_NE(nullptr, TC_idx);
    ASSERT_NE(nullptr, T_idx);
    ASSERT_NE(nullptr, A);
    ASSERT_NE(nullptr, x0);
    ASSERT_NE(nullptr, x_exp);
    ASSERT_NE(nullptr, b);
    ASSERT_NE(nullptr, tmp1);
    ASSERT_NE(nullptr, tmp2);

    ASSERT_EQ(0, l1c_setup_matrix_transforms(n, m, A, &ax_funs));
  }
  void TearDown() override {
    ax_funs.destroy();
    l1c_free_double(x0);
    l1c_free_double(x_exp);
    l1c_free_double(b);
    l1c_free_double(A);
    l1c_free_double(tmp1);
    l1c_free_double(tmp2);
    free(T_idx);
    free(TC_idx);
    cJSON_Delete(json_data);
  }

  double* x0;
  double* b;
  double* x_exp;
  double* A;
  double* tmp1;
  double* tmp2;
  int* T_idx;
  int* TC_idx;
  l1c_AxFuns ax_funs;
  cJSON* json_data;

  double enrm1;
  double epsilon;
  int m = 0;
  int n = 0;
  int mn = 0;
  int T = 0;
  int TC = 0;
};

TEST_F(L1cNestaOneIterTest, NestaOneIter) {
  // matrix_transforms doesnt set normW yet. Would be better to compute in python.
  ax_funs.norm_W = 1;
  const l1c_NestaOpts opts = {
      .mu = 1e-5, .tol = 1e-5, .sigma = epsilon, .n_continue = 5, .verbose = 0};
  const int nesta_status = l1c_nesta(m, x0, n, b, ax_funs, opts);

  ASSERT_EQ(nesta_status, 0);
  /*
    From Stable Signal Recovery from Incomplete and Inaccurate Measurements,
    Candes, Romberg, Tao 2005, it looks like we should be able to verify
    (cir. (9)-(10)):
       a. ||x_opt||_1 <= ||x_act||_1
       b. ||Ax_opt - Ax_act||_1 <= 2*\epsilon
       c. Let h = x_opt - x_act. Let h_T = h[idx_supp], and
       h_TC = h[(1:m)!=idx_supp]. Then
       ||h_TC||_1 <= ||h_T||_1

       d. From their numerical experiments, they suggest that
          ||x_opt - x_act|| < C*eps, with C<2 (see also eq. (5)).
          with eps^2 = sigma^2(n + lambda*sqrt(2*n)). Though table 1
          shows this, I dont get the same result repeating that
          experiment, even with their software. It seems that C~=3.5
   */

  /* a. ---------------------
     Check property (a).*/
  double dnrm1_x1exp = l1c_dnorm1(m, x_exp);
  double dnrm1_xp = l1c_dnorm1(m, x0);
  ASSERT_LE(dnrm1_xp, dnrm1_x1exp);

  /* -----------------------
    b. Check property (b) */
  ax_funs.Ax(x_exp, tmp1);
  ax_funs.Ax(x0, tmp2);
  double dnrm2_yerr = 0, yerr_k = 0;
  for (int i = 0; i < m; i++) {
    yerr_k = tmp1[i] - tmp2[i];
    dnrm2_yerr += yerr_k * yerr_k;
  }
  dnrm2_yerr = std::sqrt(dnrm2_yerr);

  ASSERT_LE(dnrm2_yerr, epsilon * 2);

  /* c. ------------------
     Check property (c) */
  l1c_daxpy_z(m, -1, x0, x_exp, tmp1); // h = x0 - x
  double h_T_nrm1 = 0, h_TC_nrm1 = 0;
  for (int i = 0; i < T; i++) {
    h_T_nrm1 += std::abs(tmp1[T_idx[i]]);
  }
  for (int i = 0; i < TC; i++) {
    h_TC_nrm1 += std::abs(tmp1[TC_idx[i]]);
  }
  ASSERT_LE(h_TC_nrm1, h_T_nrm1);

  /*d. TODO: implement this check. */

  /* e. We should exit cleanly for this problem.*/
  ASSERT_EQ(0, nesta_status);
}

struct NestaFifoFixture : testing::Test {
  void SetUp() override {
    fifo = _l1c_new_fmean_fifo();
    ASSERT_NE(fifo.f_vals, nullptr);
  }
  void TearDown() override { free(fifo.f_vals); }

  l1c_fmean_fifo fifo;
};

TEST_F(NestaFifoFixture, NewFmeanFifo) {
  /*Just make sure this doesnt segfault.*/
  fifo.f_vals[L1C_NESTA_NMEAN - 1] = 1;
  ASSERT_EQ(fifo.f_vals, fifo.next);
}

TEST_F(NestaFifoFixture, PushFmeansFifo) {
  int i = 0;
  double val;

  for (i = 0; i < L1C_NESTA_NMEAN + 1; i++) {
    val = (double)i;
    _l1c_push_fmeans_fifo(&fifo, val);
  }

  ASSERT_EQ(L1C_NESTA_NMEAN, fifo.n_total);

  ASSERT_DOUBLE_EQ(fifo.f_vals[0], val);
}

TEST_F(NestaFifoFixture, MeanFmeanFifo) {
  int i = 0;
  double val;
  double fbar_exp = 0, fbar = 0;

  /* Ensure this works right for n_total < L1C_NESTA_NMEAN*/
  for (i = 0; i < 4; i++) {
    val = (double)i;
    _l1c_push_fmeans_fifo(&fifo, val);
  }
  fbar = _l1c_mean_fmean_fifo(&fifo);
  /*mean([0, 1, 2, 3]) = 6/4 = 1.5*/
  fbar_exp = 1.5;
  ASSERT_NEAR(fbar, fbar_exp, kDoubleTol);

  /* ----------------------------------------------------
    Ensure this works right for n_total = L1C_NESTA_NMEAN
  */
  l1c_init_vec(L1C_NESTA_NMEAN, fifo.f_vals, 0);
  fifo.next = fifo.f_vals;

  for (i = 0; i < L1C_NESTA_NMEAN; i++) {
    val = (double)i;
    _l1c_push_fmeans_fifo(&fifo, val);
  }
  fbar = _l1c_mean_fmean_fifo(&fifo);
  /*mean([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]) = 45/10 = 4.5 */
  fbar_exp = 4.5;
  ASSERT_NEAR(fbar, fbar_exp, kDoubleTol);

  /* ----------------------------------------------------
     Ensure this works right for n_total == L1C_NESTA_NMEAN
     and when we have circled around.
  */
  l1c_init_vec(L1C_NESTA_NMEAN, fifo.f_vals, 0);
  fifo.next = fifo.f_vals;

  for (i = 0; i < L1C_NESTA_NMEAN + 2; i++) {
    val = (double)i;
    _l1c_push_fmeans_fifo(&fifo, val);
  }
  fbar = _l1c_mean_fmean_fifo(&fifo);
  /*mean([10, 11, 2, 3, 4, 5, 6, 7, 8, 9]) = 65/10 = 6.5 */
  fbar_exp = 6.5;
  ASSERT_NEAR(fbar, fbar_exp, kDoubleTol);
}

struct NestaSetupFixture : ::testing::Test {
  void SetUp() override {
    b = l1c_calloc_double(n);
    A = l1c_calloc_double(n * m);
    ASSERT_NE(nullptr, b);
    ASSERT_NE(nullptr, A);

    ASSERT_EQ(0, l1c_setup_matrix_transforms(n, m, A, &ax_funs));

    NP = _l1c_NestaProb_new(ax_funs);
    ASSERT_NE(NP, nullptr);

    for (int i = 0; i < n; i++) {
      b[i] = ((double)rand()) / (double)RAND_MAX;
    }
    for (int i = 0; i < n * m; i++) {
      A[i] = ((double)rand()) / (double)RAND_MAX;
    }
  }
  void TearDown() override {
    l1c_free_nesta_problem(NP);
    ax_funs.destroy();
    l1c_free_double(b);
    l1c_free_double(A);
  }
  double* A;
  double* b;
  int test_status = 0;
  int n = 5;
  int m = 10;

  double beta_mu = 0, beta_tol = 0;
  double sigma = 1e-3, mu = 1e-5;
  double tol = 1e-3;

  int n_continue = 5;
  DctMode dct_mode = dct1;
  l1c_AxFuns ax_funs;
  l1c_NestaProb* NP = NULL;
  l1c_NestaOpts opts = {
      .mu = mu, .tol = tol, .sigma = sigma, .n_continue = 5, .verbose = 0};
};

TEST_F(NestaSetupFixture, NestaSetupInconsistentArgs) {
  // We are checking that setup will fail for ax_funs without analysis opertator.
  ax_funs.Wz = NULL;
  int status = l1c_nesta_setup(NP, &beta_mu, &beta_tol, b, ax_funs, &opts);

  ASSERT_EQ(status, L1C_INCONSISTENT_ARGUMENTS);
}

TEST_F(NestaSetupFixture, NestaSetupSuccess) {
  int pix_idx[5] = {1, 3, 4, 6, 8};
  ASSERT_EQ(0, l1c_setup_dct_transforms(n, m, 1, dct_mode, pix_idx, &ax_funs));

  ASSERT_EQ(l1c_nesta_setup(NP, &beta_mu, &beta_tol, b, ax_funs, &opts), L1C_SUCCESS);

  ASSERT_EQ(b, NP->b);
  ASSERT_EQ(n_continue, NP->n_continue);
  ASSERT_DOUBLE_EQ(NP->sigma, sigma);
  ASSERT_DOUBLE_EQ(NP->mu, mu);
  ASSERT_DOUBLE_EQ(NP->tol, tol);

  double mu_j = NP->mu_j, tol_j = NP->tol_j;

  for (int i = 0; i < n_continue; i++) {
    mu_j *= beta_mu;
    tol_j *= beta_tol;
  }

  ASSERT_NEAR(mu_j, mu, kDoubleTol);
  ASSERT_NEAR(tol_j, tol, kDoubleTol);
}

} // namespace l1c
