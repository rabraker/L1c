/*
 * This is a test suite for the l1qc_newton library.
 */
#include "cblas.h"
#include <cjson/cJSON.h>
#include <cmath>
#include <gtest/gtest.h>

#include "common.h"
#include "json_utils.h"
#include "l1c.h"
#include "l1qc_newton.h"

#include "l1c/cgsolve.hpp"
#include "l1c_math.h"
#include "test_config.h"

extern "C" {
int _l1c_l1qcProb_new(l1c_l1qcProb* Prb,
                      int m,
                      int n,
                      double* b,
                      l1c_L1qcOpts params,
                      l1c_AxFuns ax_funs);
void _l1c_l1qcProb_delete(l1c_l1qcProb* Prb);
}

namespace l1c {

/* Initialize l1c_L1qcOpts struct. Even though for some tests
   we dont need all of them set, we should set them to clean up the ouput
   of valgrind when tracking down other problems.
*/
inline l1c_L1qcOpts init_l1qc_opts() {
  l1c_L1qcOpts params;
  params.epsilon = 1e-3;
  params.tau = 10;
  params.mu = 10;
  params.newton_tol = 1e-3;
  params.newton_max_iter = 50;
  params.lbiter = 0;
  params.lbtol = 1e-4;
  params.l1_tol = 0;
  params.verbose = 2;
  params.cg_verbose = 0;
  params.cg_tol = 1e-8;
  params.cg_maxiter = 200;
  params.warm_start_cg = 0;
  return params;
}

struct L1qcFixture : testing::Test {
  void SetUp() override {
    std::string fpath_generic = std::string(kTestDataDir) + "/l1qc_data.json";
    cJSON* json_data;

    int m_tmp = 0, n_tmp = 0;
    l1c_AxFuns ax_funs;

    ASSERT_EQ(0, load_file_to_json(fpath_generic.c_str(), &json_data)) << fpath_generic;

    ASSERT_EQ(0, extract_json_int(json_data, "m", &m));

    ASSERT_EQ(0, extract_json_int(json_data, "n", &n));
    ASSERT_EQ(0, extract_json_double(json_data, "tau0", &tau0));
    ASSERT_EQ(0, extract_json_double(json_data, "lbtol", &lbtol));
    ASSERT_EQ(0, extract_json_double(json_data, "mu", &mu));
    ASSERT_EQ(0, extract_json_double(json_data, "epsilon", &epsilon));
    ASSERT_EQ(0, extract_json_double(json_data, "cgtol", &cgtol));
    ASSERT_EQ(0, extract_json_int(json_data, "cgmaxiter", &cgmaxiter));
    ASSERT_EQ(0, extract_json_int_array(json_data, "pix_idx", &pix_idx, &m_tmp));

    ASSERT_EQ(0, extract_json_double_array(json_data, "A", &A, &m_tmp));
    ASSERT_EQ(0, extract_json_double_array(json_data, "b", &b, &n_tmp));
    ASSERT_EQ(0, extract_json_double_array(json_data, "x", &x, &m_tmp));

    ASSERT_EQ(0, extract_json_double(json_data, "tau", &tau));
    ASSERT_EQ(0, extract_json_int(json_data, "lbiter", &lbiter));
    ASSERT_EQ(0, extract_json_double_array(json_data, "u", &u, &m_tmp));
    ASSERT_EQ(0, extract_json_double_array(json_data, "r", &r, &n_tmp));
    ASSERT_EQ(0, extract_json_double(json_data, "f", &f));
    ASSERT_EQ(0, extract_json_double(json_data, "fe", &fe));
    ASSERT_EQ(0, extract_json_double_array(json_data, "fu1", &fu1, &m_tmp));
    ASSERT_EQ(0, extract_json_double_array(json_data, "fu2", &fu2, &m_tmp));
    // Smax
    ASSERT_EQ(0, extract_json_double_array(json_data, "dx_rand1", &dx_rand1, &m_tmp));
    ASSERT_EQ(0, extract_json_double_array(json_data, "du_rand1", &du_rand1, &m_tmp));
    ASSERT_EQ(0, extract_json_double(json_data, "smax", &smax));
    // h11p
    ASSERT_EQ(0, extract_json_double_array(json_data, "sigx_rand", &sigx_rand, &m_tmp));
    ASSERT_EQ(0, extract_json_double_array(json_data, "z_rand", &z_rand, &m_tmp));
    ASSERT_EQ(0, extract_json_double_array(json_data, "r_rand", &r_rand, &m_tmp));
    ASSERT_EQ(0, extract_json_double_array(json_data, "at_rrand", &at_rrand, &m_tmp));
    ASSERT_EQ(0, extract_json_double(json_data, "fe_rand", &fe_rand));
    ASSERT_EQ(0, extract_json_double_array(json_data, "h11p_z", &h11p_z, &m_tmp));
    // For descent data

    ASSERT_EQ(0, extract_json_double_array(json_data, "gradf", &gradf, &m_tmp));
    ASSERT_EQ(0, extract_json_double_array(json_data, "atr", &atr, &m_tmp));
    ASSERT_EQ(0, extract_json_double_array(json_data, "ntgx", &ntgx, &m_tmp));
    ASSERT_EQ(0, extract_json_double_array(json_data, "ntgu", &ntgu, &m_tmp));
    ASSERT_EQ(0, extract_json_double_array(json_data, "sig11", &sig11, &m_tmp));
    ASSERT_EQ(0, extract_json_double_array(json_data, "sig12", &sig12, &m_tmp));
    ASSERT_EQ(0, extract_json_double_array(json_data, "w1p", &w1p, &m_tmp));
    ASSERT_EQ(0, extract_json_double_array(json_data, "sigx", &sigx, &m_tmp));
    ASSERT_EQ(0, extract_json_double_array(json_data, "dx", &dx, &m_tmp));
    ASSERT_EQ(0, extract_json_double_array(json_data, "du", &du, &m_tmp));

    params.epsilon = epsilon;
    params.tau = tau;
    params.mu = mu;
    params.lbtol = lbtol;
    params.lbiter = lbiter;

    ASSERT_EQ(0, l1c_dct1_setup(n, m, pix_idx, &ax_funs));
    ASSERT_EQ(0, _l1c_l1qcProb_new(&(l1qc_problem), m, n, b, params, ax_funs));

    cJSON_Delete(json_data);
  }

  void TearDown() override {
    free(pix_idx);
    l1c_free_double(A);
    l1c_free_double(b);
    l1c_free_double(x);

    l1c_free_double(u);
    l1c_free_double(r);
    l1c_free_double(fu1);
    l1c_free_double(fu2);
    // for smax
    l1c_free_double(dx_rand1);
    l1c_free_double(du_rand1);
    // For h11p
    l1c_free_double(sigx_rand);
    l1c_free_double(z_rand);
    l1c_free_double(r_rand);
    l1c_free_double(at_rrand);
    l1c_free_double(h11p_z);
    // For descent data
    l1c_free_double(gradf);
    l1c_free_double(atr);
    l1c_free_double(ntgx);
    l1c_free_double(ntgu);
    l1c_free_double(sig11);
    l1c_free_double(sig12);
    l1c_free_double(w1p);
    l1c_free_double(sigx);
    l1c_free_double(dx);
    l1c_free_double(du);

    _l1c_l1qcProb_delete(&(l1qc_problem));

    l1qc_problem.ax_funs.destroy();
  }
  l1c_L1qcOpts params = init_l1qc_opts();

  int m;
  int n;
  double tau0;
  double lbtol;
  double mu;
  double epsilon;
  double cgtol;
  int cgmaxiter;
  int* pix_idx;
  double* A;
  double* b;
  double* x;
  double tau;
  int lbiter;
  double* u;
  double* r;
  double f;
  double fe;
  double* fu1;
  double* fu2;
  // for smax
  double* dx_rand1;
  double* du_rand1;
  double smax;
  // For h11p
  double* sigx_rand;
  double* z_rand;
  double* r_rand;
  double* at_rrand;
  double fe_rand;
  double* h11p_z;
  // For descent data
  double* gradf;
  double* atr;
  double* ntgx;
  double* ntgu;
  double* sig11;
  double* sig12;
  double* w1p;
  double* sigx;
  double* dx;
  double* du;

  l1c_l1qcProb l1qc_problem;
};

struct NewtonInitFixture : L1qcFixture {
  void SetUp() override {
    L1qcFixture::SetUp();
    u = l1c_malloc_double(m);
    ASSERT_NE(nullptr, u);
  }
  void TearDown() override { l1c_free_double(u); }
  double* u = NULL;
};

TEST_F(NewtonInitFixture, test_newton_init) {
  ASSERT_EQ(0, _l1c_l1qc_newton_init(m, x, u, &params));
  ASSERT_CVEC_NEAR(m, u, u, kDoubleTolSuper);
  ASSERT_NEAR(tau, params.tau, kDoubleTolSuper * 100);
  ASSERT_EQ(lbiter, params.lbiter);

  params.lbiter = 1;
  ASSERT_EQ(_l1c_l1qc_newton_init(m, x, u, &params), 0);
  ASSERT_EQ(1, params.lbiter);
}

TEST_F(L1qcFixture, test_l1qc_descent_dir) {
  l1c_l1qcProb l1qc_prob = l1qc_problem;
  l1c_CgParams cgp = {.verbose = 0, .max_iter = cgmaxiter, .tol = cgtol};
  l1c_CgResults cgr = {.cgres = 0.0, .cgiter = 0};

  cblas_dcopy(n, r, 1, l1qc_prob.r, 1);
  cblas_dcopy(m, fu1, 1, l1qc_prob.fu1, 1);
  cblas_dcopy(m, fu2, 1, l1qc_prob.fu2, 1);

  l1qc_prob.fe_val = fe;
  l1qc_prob.tau = tau0;

  /* We must initialize l1qc_prob.dx, because we have enabled warm starting*/
  l1c_init_vec(m, l1qc_prob.dx, 0.0);

  _l1c_l1qc_descent_dir(&l1qc_prob, cgp, &cgr);

  /*The next three should already be checked by test_get_gradient, but we can
   do it here too, to make sure things are staying sane.*/
  ASSERT_CVEC_NEAR(m, sig11, l1qc_prob.sig11, kDoubleTol * 100);
  ASSERT_CVEC_NEAR(m, sig12, l1qc_prob.sig12, kDoubleTol * 100);
  ASSERT_CVEC_NEAR(m, w1p, l1qc_prob.w1p, kDoubleTol * 100);
  ASSERT_CVEC_NEAR(m, dx, l1qc_prob.dx, kDoubleTol * 100);
  ASSERT_CVEC_NEAR(m, du, l1qc_prob.du, kDoubleTol * 100);
}

struct H11pfulFixture : L1qcFixture {
  void SetUp() override {
    L1qcFixture::SetUp();
    h11p_z = l1c_malloc_double(m);
    z_orig = l1c_malloc_double(m);
    h11p_data.Dwork_1m = l1c_malloc_double(m);
    ASSERT_NE(h11p_z, nullptr);
    ASSERT_NE(z_orig, nullptr);
    ASSERT_NE(h11p_data.Dwork_1m, nullptr);
  }

  void TearDown() override {
    L1qcFixture::TearDown();
    l1c_free_double(h11p_z);
    l1c_free_double(z_orig);
    l1c_free_double(h11p_data.Dwork_1m);
  }
  Hess_data h11p_data;
  double* h11p_z;
  double* z_orig;
};

TEST_F(H11pfulFixture, H11pfun) {
  h11p_data.one_by_fe = 1.0 / fe_rand;
  h11p_data.one_by_fe_sqrd = 1.0 / (fe_rand * fe_rand);
  h11p_data.atr = at_rrand;
  h11p_data.sigx = sigx_rand;

  h11p_data.AtAx = l1qc_problem.ax_funs.AtAx;

  cblas_dcopy(m, z_rand, 1, z_orig, 1);

  _l1c_l1qc_H11pfun(m, z_rand, h11p_z, &h11p_data);

  ASSERT_CVEC_NEAR(m, h11p_z, h11p_z, kDoubleTol);
  // Ensure we didnt overwrite data in z.
  ASSERT_CVEC_NEAR(m, z_rand, z_orig, kDoubleTolSuper);
}

struct L1qcHessGradFixture : L1qcFixture {
  void SetUp() override {
    L1qcFixture::SetUp();
    sigx = l1c_calloc_double(m);
    ASSERT_NE(nullptr, sigx);
  }
  void TearDown() override {
    L1qcFixture::TearDown();
    l1c_free_double(sigx);
  }

  double* sigx;
};

TEST_F(L1qcHessGradFixture, L1qcHessGrad) {
  cblas_dcopy(m, fu1, 1, l1qc_problem.fu1, 1);
  cblas_dcopy(m, fu2, 1, l1qc_problem.fu2, 1);

  l1qc_problem.fe_val = fe;
  l1qc_problem.tau = tau0;

  /*-------------- Compute --------------------------- */
  _l1c_l1qc_hess_grad(&l1qc_problem, sigx, atr);

  /* ----- Check -------*/
  ASSERT_CVEC_NEAR(m, sig11, l1qc_problem.sig11, kDoubleTolSuper * 100);
  ASSERT_CVEC_NEAR(m, sig12, l1qc_problem.sig12, kDoubleTolSuper * 100);
  ASSERT_CVEC_NEAR(m, w1p, l1qc_problem.w1p, kDoubleTolSuper * 100);
  ASSERT_CVEC_NEAR(m, sigx, sigx, kDoubleTolSuper * 100);
  ASSERT_CVEC_NEAR(m, ntgu, l1qc_problem.ntgu, kDoubleTolSuper * 100);
  ASSERT_CVEC_NEAR(m * 2, gradf, l1qc_problem.gradf, kDoubleTolSuper * 100);
}

TEST_F(L1qcFixture, FindMaxStep) {
  cblas_dcopy(m, dx_rand1, 1, l1qc_problem.dx, 1);
  cblas_dcopy(m, du_rand1, 1, l1qc_problem.du, 1);
  cblas_dcopy(m, fu1, 1, l1qc_problem.fu1, 1);
  cblas_dcopy(m, fu2, 1, l1qc_problem.fu2, 1);
  cblas_dcopy(n, r, 1, l1qc_problem.r, 1);

  l1qc_problem.epsilon = epsilon;
  l1qc_problem.m = m;
  l1qc_problem.n = n;

  smax = _l1c_l1qc_find_max_step(&l1qc_problem);
  ASSERT_NEAR(smax, smax, kDoubleTol);
}

TEST_F(L1qcFixture, test_f_eval) {
  l1qc_problem.tau = tau0;
  l1qc_problem.epsilon = epsilon;

  /* ------------- Compute----------------------*/
  _l1c_l1qc_f_eval(&l1qc_problem, x, u);

  /* ------------- check -----------------------*/
  ASSERT_NEAR(fe, l1qc_problem.fe_val, kDoubleTol);
  ASSERT_CVEC_NEAR(m, fu1, l1qc_problem.fu1, kDoubleTol);
  ASSERT_CVEC_NEAR(m, fu2, l1qc_problem.fu2, kDoubleTol);
  ASSERT_CVEC_NEAR(n, r, l1qc_problem.r, kDoubleTol);

  ASSERT_NEAR(f, l1qc_problem.f_val, kDoubleTol);
}

struct FeasibleStartFixture : testing::Test {
  void SetUp() override {
    std::string fpath_1iter = std::string(kTestDataDir) + "/lb_test_data_AX.json";

    ASSERT_EQ(0, load_file_to_json(fpath_1iter.c_str(), &json_data)) << fpath_1iter;

    ASSERT_EQ(0, extract_json_double_array(json_data, "x0", &x0, &m));
    ASSERT_EQ(0, extract_json_double_array(json_data, "b", &b, &n));
    ASSERT_EQ(0, extract_json_double_array(json_data, "A", &A, &mn));

    ASSERT_EQ(mn, m * n);
    ASSERT_NE(nullptr, A);
    ASSERT_NE(nullptr, x0);
    ASSERT_NE(nullptr, b);

    ASSERT_EQ(0, l1c_setup_matrix_transforms(n, m, A, &ax_funs));
    ASSERT_EQ(0, _l1c_l1qcProb_new(&problem, m, n, b, params, ax_funs));
  }

  void TearDown() override {
    l1c_free_double(A);
    l1c_free_double(x0);
    l1c_free_double(b);
    _l1c_l1qcProb_delete(&problem);
    ax_funs.destroy();
    cJSON_Delete(json_data);
  }
  cJSON* json_data;
  double* x0;
  double* b;
  double* A;

  int is_feasible = 0;
  int m = 0;
  int n = 0;
  int mn = 0;
  l1c_AxFuns ax_funs;
  l1c_l1qcProb problem;
  l1c_L1qcOpts params = init_l1qc_opts();
};

TEST_F(FeasibleStartFixture, test_l1qc_check_feasible_start) {
  problem.epsilon = 0.0;
  is_feasible = _l1c_l1qc_check_feasible_start(&problem, x0);
  ASSERT_EQ(is_feasible, L1C_INFEASIBLE_START);

  problem.epsilon = 1.0;
  is_feasible = _l1c_l1qc_check_feasible_start(&problem, x0);
  ASSERT_EQ(is_feasible, 0);
}

struct TestL1qcNewtonFixture : testing::Test {

  void SetUp() override {
    std::string fpath = std::string(kTestDataDir) + "/lb_test_data_AX.json";
    ASSERT_EQ(0, load_file_to_json(fpath.c_str(), &json_data)) << fpath;

    ASSERT_EQ(0, extract_json_double_array(json_data, "x0", &x0, &n));
    ASSERT_EQ(0, extract_json_double_array(json_data, "x_act", &x_exp, &n));
    ASSERT_EQ(0, extract_json_double_array(json_data, "b", &b, &m));

    ASSERT_EQ(0, extract_json_double(json_data, "epsilon", &params.epsilon));
    ASSERT_EQ(0, extract_json_double(json_data, "mu", &params.mu));
    ASSERT_EQ(0, extract_json_double(json_data, "lbtol", &params.lbtol));
    ASSERT_EQ(0, extract_json_double(json_data, "newtontol", &params.newton_tol));
    ASSERT_EQ(0, extract_json_int(json_data, "newtonmaxiter", &params.newton_max_iter));
    ASSERT_EQ(0, extract_json_double(json_data, "cgtol", &params.cg_tol));
    ASSERT_EQ(0, extract_json_double(json_data, "enrm1", &enrm1));
    ASSERT_EQ(0, extract_json_int(json_data, "cgmaxiter", &params.cg_maxiter));

    ASSERT_EQ(0, extract_json_int_array(json_data, "T_idx", &T_idx, &T));
    ASSERT_EQ(0, extract_json_int_array(json_data, "TC_idx", &TC_idx, &TC));
    ASSERT_EQ(0, extract_json_double_array(json_data, "A", &A, &mn));

    ASSERT_EQ(mn, m * n);

    tmp1 = l1c_calloc_double(n);
    tmp2 = l1c_calloc_double(n);
    ASSERT_NE(nullptr, TC_idx);
    ASSERT_NE(nullptr, T_idx);
    ASSERT_NE(nullptr, A);
    ASSERT_NE(nullptr, x0);
    ASSERT_NE(nullptr, x_exp);
    ASSERT_NE(nullptr, b);
    ASSERT_NE(nullptr, tmp1);
    ASSERT_NE(nullptr, tmp2);

    ASSERT_EQ(0, l1c_setup_matrix_transforms(m, n, A, &ax_funs));
  }
  void TearDown() override {
    ax_funs.destroy();
    l1c_free_double(x0);
    l1c_free_double(x_exp);
    l1c_free_double(b);
    l1c_free_double(A);
    l1c_free_double(tmp1);
    l1c_free_double(tmp2);
    free(T_idx);
    free(TC_idx);
    cJSON_Delete(json_data);
  }
  cJSON* json_data;
  double* tmp1;
  double* tmp2;
  double* x0;
  double* b;
  double* x_exp;
  double* A;
  int* T_idx;
  int* TC_idx;

  double enrm1;

  int m = 0;
  int n = 0;
  int mn = 0;
  int T = 0;
  int TC = 0;

  l1c_AxFuns ax_funs;
  l1c_LBResult lb_res;
  l1c_L1qcOpts params = init_l1qc_opts();
};

TEST_F(TestL1qcNewtonFixture, test_l1qc_newton) {
  params.verbose = 0;
  params.warm_start_cg = 0;
  params.l1_tol = 1e-4; // wont be used.

  lb_res = l1c_l1qc_newton(n, x0, m, b, params, ax_funs);

  /*
    From Stable Signal Recovery from Incomplete and Inaccurate Measurements,
    Candes, Romberg, Tao 2005, it looks like we should be able to verify
    (cir. (9)-(10)):
       a. ||x_opt||_1 <= ||x_act||_1
       b. ||Ax_opt - Ax_act||_1 <= 2*\epsilon
       c. Let h = x_opt - x_act. Let h_T = h[idx_supp], and
       h_TC = h[(1:m)!=idx_supp]. Then
       ||h_TC||_1 <= ||h_T||_1

       d. From their numerical experiments, they suggest that
          ||x_opt - x_act|| < C*eps, with C<2 (see also eq. (5)).
          with eps^2 = sigma^2(n + lambda*sqrt(2*n)). Though table 1
          shows this, I dont get the same result repeating that
          experiment, even with their software. It seems that C~=3.5
   */

  /* a. ---------------------
     Check property (a).*/
  double dnrm1_x1exp = l1c_dnorm1(n, x_exp);
  double dnrm1_xp = l1c_dnorm1(n, x0);
  ASSERT_LT(dnrm1_xp, dnrm1_x1exp);

  /* -----------------------
    b. Check property (b) */
  ax_funs.Ax(x_exp, tmp1);
  ax_funs.Ax(x0, tmp2);
  double dnrm2_yerr = 0, yerr_k = 0;
  for (int i = 0; i < n; i++) {
    yerr_k = tmp1[i] - tmp2[i];
    dnrm2_yerr += yerr_k * yerr_k;
  }
  dnrm2_yerr = std::sqrt(dnrm2_yerr);

  ASSERT_LT(dnrm2_yerr, params.epsilon * 2);

  /* c. ------------------
     Check property (c) */
  l1c_daxpy_z(n, 1, x0, x_exp, tmp1); // h = x0 - x
  double h_T_nrm1 = 0, h_TC_nrm1 = 0;
  for (int i = 0; i < T; i++) {
    h_T_nrm1 += std::abs(tmp1[T_idx[i]]);
  }
  for (int i = 0; i < TC; i++) {
    h_TC_nrm1 += std::abs(tmp1[TC_idx[i]]);
  }
  ASSERT_LT(h_TC_nrm1, h_T_nrm1);

  /*d. TODO: implement this check. */

  /* e. We should exit cleanly for this problem.*/
  ASSERT_EQ(0, lb_res.status);
}

TEST(L1qc, test_newton_init_regres1) {
  l1c_L1qcOpts params = init_l1qc_opts();
  int m = 4;

  double x[] = {1.0, 2.0, 3.0, 4.0};
  double u[] = {0, 0, 0, 0};
  double u_exp[] = {1.3500, 2.3000, 3.2500, 4.2000};

  params.lbtol = 0.1;
  params.mu = 0.1;

  _l1c_l1qc_newton_init(m, x, u, &params);

  ASSERT_CVEC_NEAR(m, u_exp, u, kDoubleTolSuper);
}

} // namespace l1c
