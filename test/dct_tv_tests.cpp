///
/// Tests for the 1D DCT-based transforms.
///
#include <cmath>
#include <stdlib.h>

#include <cjson/cJSON.h>
#include <glog/logging.h>
#include <gtest/gtest.h>

#include "common.h"
#include "json_utils.h"
#include "l1c.h"
#include "test_config.h"

namespace l1c {

struct DctTvData {
  DctTvData() = default;
  DctTvData(const DctTvData&) = delete;
  DctTvData(DctTvData&&) = delete;
  DctTvData operator=(DctTvData&&) = delete;
  DctTvData operator=(const DctTvData&) = delete;

  int* pix_idx;
  double* MtRty_RMx_exp;
  double* MtRty_RMx_act;
  double* MtRty_act;
  double* MtRty_exp;
  double* RMx_act;
  double* RMx_exp;

  double* Mx_act;
  double* Mx_exp;
  double* Mty_act;
  double* Mty_exp;

  double* Rx_act;
  double* Rx_exp;
  double* Rty_act;
  double* Rty_exp;

  double* x_in;
  double* y_in;
  double* z_in;

  double alp_v;
  double alp_h;
  /* Transform is n by mtot. mtot = mrow*mcol.*/
  int mrow;
  int mcol;
  int mtot;
  int p;
  int n;
  BpMode bp_mode;
  DctMode dct_mode;
  int setup_status;
  std::string fpath;

  ~DctTvData() {
    l1c_free_double(x_in);
    l1c_free_double(y_in);
    l1c_free_double(z_in);

    l1c_free_double(MtRty_RMx_exp);
    l1c_free_double(MtRty_exp);
    l1c_free_double(RMx_exp);
    l1c_free_double(Mx_exp);
    l1c_free_double(Mty_exp);
    l1c_free_double(Rx_exp);
    l1c_free_double(Rty_exp);

    l1c_free_double(MtRty_RMx_act);
    l1c_free_double(MtRty_act);
    l1c_free_double(RMx_act);
    l1c_free_double(Mx_act);
    l1c_free_double(Mty_act);
    l1c_free_double(Rx_act);
    l1c_free_double(Rty_act);

    free(pix_idx);
  }
};

/* Global variable for all test cases.
 */
class DctTvTestsAll : public testing::Test {
public:
  DctTvData dctd_;
  l1c_AxFuns ax_funs_;

  void SetUpImpl(const std::string& fname, BpMode bp_mode, DctMode dct_mode) {

    cJSON* test_data_json;
    dctd_.fpath = std::string(kTestDataDir) + "/" + fname;

    CHECK_EQ(0, load_file_to_json(dctd_.fpath.c_str(), &test_data_json)) << dctd_.fpath;

    dctd_.bp_mode = bp_mode;
    dctd_.dct_mode = dct_mode;

    CHECK_EQ(
        0, extract_json_double_array(test_data_json, "x_in", &dctd_.x_in, &dctd_.mtot));
    CHECK_EQ(0,
             extract_json_double_array(test_data_json, "y_in", &dctd_.y_in, &dctd_.n));
    CHECK_EQ(0,
             extract_json_double_array(test_data_json, "z_in", &dctd_.z_in, &dctd_.p));

    CHECK_EQ(0,
             extract_json_double_array(
                 test_data_json, "MtEt_EMx", &dctd_.MtRty_RMx_exp, &dctd_.p));

    CHECK_EQ(
        0,
        extract_json_double_array(test_data_json, "MtEty", &dctd_.MtRty_exp, &dctd_.p));

    CHECK_EQ(
        0, extract_json_double_array(test_data_json, "EMx", &dctd_.RMx_exp, &dctd_.n));

    CHECK_EQ(
        0, extract_json_double_array(test_data_json, "Mx", &dctd_.Mx_exp, &dctd_.mtot));
    CHECK_EQ(
        0, extract_json_double_array(test_data_json, "Mty", &dctd_.Mty_exp, &dctd_.p));
    CHECK_EQ(0,
             extract_json_double_array(test_data_json, "Ex", &dctd_.Rx_exp, &dctd_.n));
    CHECK_EQ(
        0,
        extract_json_double_array(test_data_json, "Ety", &dctd_.Rty_exp, &dctd_.mtot));

    CHECK_EQ(0, extract_json_double(test_data_json, "alp_v", &dctd_.alp_v));
    CHECK_EQ(0, extract_json_double(test_data_json, "alp_h", &dctd_.alp_h));

    CHECK_EQ(
        0, extract_json_int_array(test_data_json, "pix_idx", &dctd_.pix_idx, &dctd_.n));
    CHECK_EQ(0, extract_json_int(test_data_json, "mrow", &dctd_.mrow));
    CHECK_EQ(0, extract_json_int(test_data_json, "mcol", &dctd_.mcol));
    CHECK_EQ(0, extract_json_int(test_data_json, "p", &dctd_.p));

    CHECK_EQ(dctd_.mrow * dctd_.mcol, dctd_.mtot);

    dctd_.MtRty_RMx_act = l1c_malloc_double(dctd_.p);
    dctd_.MtRty_act = l1c_malloc_double(dctd_.p);
    dctd_.RMx_act = l1c_malloc_double(dctd_.mtot);
    dctd_.Mx_act = l1c_malloc_double(dctd_.mtot);
    dctd_.Mty_act = l1c_malloc_double(dctd_.p);
    dctd_.Rx_act = l1c_calloc_double(dctd_.n);
    dctd_.Rty_act = l1c_malloc_double(dctd_.mtot);

    int status_setup = l1c_setup_dctTV_transforms(dctd_.n,
                                                  dctd_.mrow,
                                                  dctd_.mcol,
                                                  dctd_.alp_v,
                                                  dctd_.alp_h,
                                                  dct_mode,
                                                  bp_mode,
                                                  dctd_.pix_idx,
                                                  &ax_funs_);
    CHECK_EQ(status_setup, L1C_SUCCESS);

    check_ax_fun_properties();

    cJSON_Delete(test_data_json);
  }
  void check_ax_fun_properties() {
    CHECK_EQ(ax_funs_.n, dctd_.n);

    CHECK_EQ(ax_funs_.q, dctd_.mtot);
    if (dctd_.bp_mode == synthesis) {
      CHECK_EQ(ax_funs_.m, dctd_.p);
      CHECK_EQ(ax_funs_.p, dctd_.mtot);
    } else {
      CHECK_EQ(ax_funs_.m, dctd_.mtot);
      CHECK_EQ(ax_funs_.p, dctd_.p);
    }

    CHECK_EQ(ax_funs_.data, nullptr);
  }

  void TearDown() override { ax_funs_.destroy(); }
};

class DctTvSynthesisTests : public DctTvTestsAll,
                            public ::testing::WithParamInterface<std::string> {
public:
  void SetUp() override {
    const BpMode bp_mode = synthesis;
    const DctMode dct_mode = dct2;
    const std::string test_data = GetParam();
    SetUpImpl(test_data, bp_mode, dct_mode);
  }
};

INSTANTIATE_TEST_SUITE_P(DctTvTestCases,
                         DctTvSynthesisTests,
                         ::testing::Values("dct2_tv_square.json",
                                           "dct2_tv_vh_square.json",
                                           "dct2_tv_v_square.json",
                                           "dct2_tv_h_square.json"));

TEST_P(DctTvSynthesisTests, TestMtRty) {
  ax_funs_.Aty(dctd_.y_in, dctd_.MtRty_act);

  ASSERT_CVEC_NEAR(dctd_.mtot, dctd_.MtRty_exp, dctd_.MtRty_act, kDoubleTolSuper);
}

TEST_P(DctTvSynthesisTests, TestRMx) {
  ax_funs_.Ax(dctd_.z_in, dctd_.RMx_act);

  ASSERT_CVEC_NEAR(dctd_.n, dctd_.RMx_exp, dctd_.RMx_act, kDoubleTolSuper);
}

TEST_P(DctTvSynthesisTests, TestMz) {
  ax_funs_.Mx(dctd_.z_in, dctd_.Mx_act);

  ASSERT_CVEC_NEAR(dctd_.mtot, dctd_.Mx_exp, dctd_.Mx_act, kDoubleTolSuper);
}

TEST_P(DctTvSynthesisTests, TestMtx) {
  /* --------- Wt ---------------------*/
  ax_funs_.Mty(dctd_.x_in, dctd_.Mty_act);

  ASSERT_CVEC_NEAR(dctd_.p, dctd_.Mty_exp, dctd_.Mty_act, kDoubleTolSuper);
}

TEST_P(DctTvSynthesisTests, TestWz) {
  /* Identity in synthesis mode.*/
  ax_funs_.Wz(dctd_.z_in, dctd_.Mx_act);

  ASSERT_CVEC_NEAR(dctd_.mtot, dctd_.z_in, dctd_.Mx_act, kDoubleTolSuper);
}

TEST_P(DctTvSynthesisTests, TestWtx) {
  /* Identity in synthesis mode.*/
  ax_funs_.Wtx(dctd_.z_in, dctd_.Mx_act);

  ASSERT_CVEC_NEAR(dctd_.mtot, dctd_.z_in, dctd_.Mx_act, kDoubleTolSuper);
}

TEST_P(DctTvSynthesisTests, TestRx) {
  ax_funs_.Rx(dctd_.x_in, dctd_.Rx_act);

  ASSERT_CVEC_NEAR(dctd_.n, dctd_.Rx_exp, dctd_.Rx_act, kDoubleTolSuper);
}

TEST_P(DctTvSynthesisTests, TestRty) {
  ax_funs_.Rty(dctd_.y_in, dctd_.Rty_act);
  ASSERT_CVEC_NEAR(dctd_.mtot, dctd_.Rty_exp, dctd_.Rty_act, kDoubleTolSuper);
}

class DctTvAnalysisTests : public DctTvTestsAll {
public:
  void SetUp() override {
    const BpMode bp_mode = analysis;
    const DctMode dct_mode = dct2;
    SetUpImpl("dct2_tv_vh_square.json", bp_mode, dct_mode);
  }
};

/* --------------------------- ANALYSIS MODE CHECKS -------------- */
TEST_F(DctTvAnalysisTests, test_Wz_analysis) {
  /* In Analysis mode, this should be the identity.*/
  ax_funs_.Wz(dctd_.z_in, dctd_.Mx_act);
  ASSERT_CVEC_NEAR(dctd_.mtot, dctd_.Mx_exp, dctd_.Mx_act, kDoubleTolSuper);
}

TEST_F(DctTvAnalysisTests, test_Wtx_analysis) {
  /* In Analysis mode, this should be the identity.*/
  ax_funs_.Wtx(dctd_.x_in, dctd_.Mty_act);
  ASSERT_CVEC_NEAR(dctd_.p, dctd_.Mty_exp, dctd_.Mty_act, kDoubleTolSuper);
}

TEST_F(DctTvAnalysisTests, test_Mx_analysis) {
  /* In Analysis mode, this should be the identity.*/
  ax_funs_.Mx(dctd_.x_in, dctd_.Mx_act);

  ASSERT_CVEC_NEAR(dctd_.mtot, dctd_.x_in, dctd_.Mx_act, kDoubleTolSuper);
}

TEST_F(DctTvAnalysisTests, test_Mty_analysis) {
  /* In Analysis mode, this should be the identity.*/
  ax_funs_.Mty(dctd_.x_in, dctd_.Mty_act);

  ASSERT_CVEC_NEAR(dctd_.mtot, dctd_.x_in, dctd_.Mty_act, kDoubleTolSuper);
}

TEST_F(DctTvAnalysisTests, test_Ax_analysis) {
  /* In Analysis mode, this should be the same as Rx.*/
  ax_funs_.Rx(dctd_.x_in, dctd_.Rx_act);

  ASSERT_CVEC_NEAR(dctd_.n, dctd_.Rx_exp, dctd_.Rx_act, kDoubleTolSuper);
}

TEST_F(DctTvAnalysisTests, test_Aty_analysis) {
  /* In Analysis mode, this should be the same as Rty.*/
  ax_funs_.Rty(dctd_.y_in, dctd_.Rty_act);

  ASSERT_CVEC_NEAR(dctd_.mtot, dctd_.Rty_exp, dctd_.Rty_act, kDoubleTolSuper);
}

TEST_P(DctTvSynthesisTests, test_normW) {
  BpMode bp_mode = analysis;
  DctMode dct_mode = dct1;
  int status = 0, n = 5, mrow = 10, mcol = 10;
  double alp_v = 4.0;
  double alp_h = 5.0;
  double normW_exp = 0;
  int pix_idx[5] = {0, 3, 4, 6, 7};

  normW_exp = std::sqrt(1 + 4 * alp_v * alp_v + 4 * alp_h * alp_h);
  status = l1c_setup_dctTV_transforms(
      n, mrow, mcol, alp_v, alp_h, dct_mode, bp_mode, pix_idx, &ax_funs_);
  ASSERT_EQ(status, L1C_SUCCESS);
  ASSERT_NEAR(ax_funs_.norm_W, normW_exp, kDoubleTolSuper);
  ax_funs_.destroy();

  alp_h = 0;
  alp_v = 0;
  status = l1c_setup_dctTV_transforms(
      n, mrow, mcol, alp_v, alp_h, dct_mode, bp_mode, pix_idx, &ax_funs_);
  ASSERT_EQ(status, L1C_SUCCESS);
  normW_exp = sqrt(1 + 4 * alp_v * alp_v + 4 * alp_h * alp_h);
  ASSERT_NEAR(ax_funs_.norm_W, normW_exp, kDoubleTolSuper);
  ax_funs_.destroy();

  /*We should always get 1.0 in synthesis mode.*/
  bp_mode = synthesis;
  status = l1c_setup_dctTV_transforms(
      n, mrow, mcol, 0.0, 0.0, dct_mode, bp_mode, pix_idx, &ax_funs_);
  ASSERT_EQ(status, L1C_SUCCESS);
  ASSERT_NEAR(ax_funs_.norm_W, 1.0, kDoubleTolSuper);
}

/* Check that the setup function returns an error code when we expect it to. */
TEST_P(DctTvSynthesisTests, test_input_errors) {
  DctMode dct_mode = dct1;
  int status = 0, n = 5, mrow = 10, mcol = 10;
  double alp_v = 1.0;
  double alp_h = 1.0;
  int pix_idx[5] = {0, 3, 4, 6, 7};
  BpMode bp_mode = analysis;
  /* alp_v < 0 should throw an error. */
  alp_v = -1.0;
  status = l1c_setup_dctTV_transforms(
      n, mrow, mcol, alp_v, alp_h, dct_mode, bp_mode, pix_idx, &ax_funs_);

  ASSERT_EQ(status, L1C_INVALID_ARGUMENT);

  /* alp_h < 0 should throw an error. */
  alp_v = 1.0;
  alp_h = -1.0;
  status = l1c_setup_dctTV_transforms(
      n, mrow, mcol, alp_v, alp_h, dct_mode, bp_mode, pix_idx, &ax_funs_);

  /* alp_v > 0, requires mrow>2, mcol>2   */
  alp_v = 1.0;
  alp_h = 0.0;
  mcol = 10;
  mrow = 1;
  status = l1c_setup_dctTV_transforms(
      n, mrow, mcol, alp_v, alp_h, dct_mode, bp_mode, pix_idx, &ax_funs_);
  ASSERT_EQ(status, L1C_INVALID_ARGUMENT);

  mrow = 10;
  mcol = 1;
  status = l1c_setup_dctTV_transforms(
      n, mrow, mcol, alp_v, alp_h, dct_mode, bp_mode, pix_idx, &ax_funs_);
  ASSERT_EQ(status, L1C_INVALID_ARGUMENT);

  /* alp_h > 0, requires mrow>2, mcol>2   */
  alp_v = 0.0;
  alp_h = 1.0;
  mcol = 10;
  mrow = 1;
  status = l1c_setup_dctTV_transforms(
      n, mrow, mcol, alp_v, alp_h, dct_mode, bp_mode, pix_idx, &ax_funs_);
  ASSERT_EQ(status, L1C_INVALID_ARGUMENT);

  mrow = 10;
  mcol = 1;
  status = l1c_setup_dctTV_transforms(
      n, mrow, mcol, alp_v, alp_h, dct_mode, bp_mode, pix_idx, &ax_funs_);
  ASSERT_EQ(status, L1C_INVALID_ARGUMENT);
}

} // namespace l1c
