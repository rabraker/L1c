#pragma once
#include <gtest/gtest.h>

static constexpr double kDoubleTol = 1e-9;
static constexpr double kDoubleTolSuper = 1e-9;

testing::AssertionResult AssertCVectorEq(const char* m_expr,
                                         const char* n_expr,
                                         const char* o_expr,
                                         const char* p_expr,
                                         int n,
                                         const double* v1,
                                         const double* v2,
                                         double tol);

#define ASSERT_CVEC_NEAR(n, a, b, tol)                                                 \
  EXPECT_PRED_FORMAT4(AssertCVectorEq, n, a, b, tol);
