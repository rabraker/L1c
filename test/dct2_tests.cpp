/*
 *  Tests for the 1D DCT-based transforms.
 */
#include "gtest/gtest.h"
#include <cjson/cJSON.h>
#include <fftw3.h>
#include <gtest/gtest.h>
#include <stdlib.h>

#include "common.h"
#include "json_utils.h"
#include "l1c.h"
#include "test_config.h"

namespace l1c {

struct Dct2Fixture : testing::TestWithParam<std::string> {
  void SetUp() override {
    cJSON* test_data_json;

    const std::string fpath = std::string(l1c::kTestDataDir) + "/" + GetParam();
    ASSERT_EQ(0, load_file_to_json(fpath.c_str(), &test_data_json)) << fpath;

    ASSERT_EQ(0, extract_json_double_array(test_data_json, "x_in", &x_in, &mtot));
    ASSERT_EQ(0, extract_json_double_array(test_data_json, "y_in", &y_in, &n));
    ASSERT_EQ(0, extract_json_double_array(test_data_json, "z_in", &z_in, &mtot));
    ASSERT_EQ(
        0,
        extract_json_double_array(test_data_json, "MtEt_EMx", &MtEty_EMx_exp, &mtot));
    ASSERT_EQ(0, extract_json_double_array(test_data_json, "MtEty", &MtEty_exp, &mtot));
    ASSERT_EQ(0, extract_json_double_array(test_data_json, "EMx", &EMx_exp, &n));
    ASSERT_EQ(0, extract_json_double_array(test_data_json, "Mx", &Mx_exp, &mtot));
    ASSERT_EQ(0, extract_json_double_array(test_data_json, "Mty", &Mty_exp, &mtot));
    ASSERT_EQ(0, extract_json_double_array(test_data_json, "Ex", &Ex_exp, &n));
    ASSERT_EQ(0, extract_json_double_array(test_data_json, "Ety", &Ety_exp, &mtot));

    ASSERT_EQ(0, extract_json_int_array(test_data_json, "pix_idx", &pix_idx, &n));
    ASSERT_EQ(0, extract_json_int(test_data_json, "mrow", &mrow));
    ASSERT_EQ(0, extract_json_int(test_data_json, "mcol", &mcol));

    ASSERT_EQ(mrow * mcol, mtot);

    MtEty_EMx_act = l1c_malloc_double(mtot);
    MtEty_act = l1c_malloc_double(mtot);
    EMx_act = l1c_malloc_double(mtot);
    Mx_act = l1c_malloc_double(mtot);
    Mty_act = l1c_malloc_double(mtot);
    Ex_act = l1c_calloc_double(n);
    Ety_act = l1c_malloc_double(mtot);

    l1c_dct2_setup(n, mrow, mcol, pix_idx, &ax_funs);

    cJSON_Delete(test_data_json);
  }

  void TearDown() override {
    l1c_free_double(x_in);
    l1c_free_double(y_in);
    l1c_free_double(z_in);

    l1c_free_double(MtEty_EMx_exp);
    l1c_free_double(MtEty_exp);
    l1c_free_double(EMx_exp);
    l1c_free_double(Mx_exp);
    l1c_free_double(Mty_exp);
    l1c_free_double(Ex_exp);
    l1c_free_double(Ety_exp);

    l1c_free_double(MtEty_EMx_act);
    l1c_free_double(MtEty_act);
    l1c_free_double(EMx_act);
    l1c_free_double(Mx_act);
    l1c_free_double(Mty_act);
    l1c_free_double(Ex_act);
    l1c_free_double(Ety_act);

    free(pix_idx);
    ax_funs.destroy();
  }

  int* pix_idx;
  double* MtEty_EMx_exp;
  double* MtEty_EMx_act;
  double* MtEty_act;
  double* MtEty_exp;
  double* EMx_act;
  double* EMx_exp;

  double* Mx_act;
  double* Mx_exp;
  double* Mty_act;
  double* Mty_exp;

  double* Ex_act;
  double* Ex_exp;
  double* Ety_act;
  double* Ety_exp;

  double* x_in;
  double* y_in;
  double* z_in;

  /* Transform is n by mtot. mtot = mrow*mcol.*/
  int mrow;
  int mcol;
  int mtot;
  int n;

  l1c_AxFuns ax_funs;
};

INSTANTIATE_TEST_SUITE_P(Dct2TestCases,
                         Dct2Fixture,
                         testing::Values("dct2_small.json",
                                         "dct2_small_tall.json",
                                         "dct2_small_wide.json",
                                         "dct2_small_pure_dct.json"));

TEST_P(Dct2Fixture, test_dct2_MtEt_EMx) {
  /* Provided and freed by setup_small() and teardown_small()*/
  ax_funs.AtAx(x_in, MtEty_EMx_act);

  ASSERT_CVEC_NEAR(mtot, MtEty_EMx_exp, MtEty_EMx_act, kDoubleTolSuper);
}

TEST_P(Dct2Fixture, test_dct2_MtEty) {
  ax_funs.Aty(y_in, MtEty_act);

  ASSERT_CVEC_NEAR(mtot, MtEty_exp, MtEty_act, kDoubleTolSuper);
}

TEST_P(Dct2Fixture, test_dct2_EMx) {
  ax_funs.Ax(x_in, EMx_act);

  ASSERT_CVEC_NEAR(n, EMx_exp, EMx_act, kDoubleTolSuper);
}

TEST_P(Dct2Fixture, test_dct2_Mx) {
  ax_funs.Mx(x_in, Mx_act);

  ASSERT_CVEC_NEAR(mtot, Mx_exp, Mx_act, kDoubleTolSuper);
}

TEST_P(Dct2Fixture, test_dct2_Wz_synth) {
  /* Should be the identity, for synthesis.*/
  ax_funs.Wz(x_in, Mx_act);

  ASSERT_CVEC_NEAR(mtot, x_in, Mx_act, kDoubleTolSuper);
}

TEST_P(Dct2Fixture, test_dct2_Wtx_synth) {
  /* Should be the identity, for synthesis.*/
  ax_funs.Wtx(x_in, Mx_act);

  ASSERT_CVEC_NEAR(mtot, x_in, Mx_act, kDoubleTolSuper);
}

TEST_P(Dct2Fixture, test_dct2_Mty) {
  ax_funs.Mty(z_in, Mty_act);

  ASSERT_CVEC_NEAR(mtot, Mty_exp, Mty_act, kDoubleTolSuper);
}

TEST_P(Dct2Fixture, test_dct2_Ex) {
  ax_funs.Rx(x_in, Ex_act);

  ASSERT_CVEC_NEAR(n, Ex_exp, Ex_act, kDoubleTolSuper);
}

TEST_P(Dct2Fixture, test_dct2_Ety) {
  ax_funs.Rty(y_in, Ety_act);

  ASSERT_CVEC_NEAR(mtot, Ety_exp, Ety_act, kDoubleTolSuper);
}

} //  namespace l1c
