#include <cmath>
#include <gtest/gtest.h>

#include "common.h"

testing::AssertionResult AssertCVectorEq(const char* n_expr,
                                         const char* a_expr,
                                         const char* b_expr,
                                         const char* tol_expr,
                                         int n,
                                         const double* A,
                                         const double* B,
                                         double tol) {
  bool failed = false;
  for (int i = 0; i < n; i++) {
    if (std::abs(A[i] - B[i]) > tol) {
      failed = true;
      break;
    }
  }
  if (!failed) {
    return testing::AssertionSuccess();
  }

  return testing::AssertionFailure()
         << "The difference between " << a_expr << " and " << b_expr << " exceeds "
         << tol_expr << " where " << a_expr << " is \n[" << A << "]\nand " << b_expr
         << "is [" << B << "]\nand " << tol_expr << " is " << tol << ", with " << n_expr
         << " = " << n;
}
