/*
  Tests for Image Gradients and Laplacian.
 */

#include <cjson/cJSON.h>
#include <gtest/gtest.h>

#include "TV.h"
#include "common.h"
#include "json_utils.h"
#include "l1c.h"
#include "test_config.h"

namespace l1c {

struct ImgDiffData {
  ImgDiffData() = default;
  ImgDiffData(const ImgDiffData&) = delete;
  ImgDiffData(ImgDiffData&&) = delete;
  ImgDiffData operator=(const ImgDiffData&) = delete;
  ImgDiffData operator=(ImgDiffData&&) = delete;

  double* A;
  double* DxA_exp;
  double* DyA_exp;

  double* DxTA_exp;
  double* DyTA_exp;

  double* DxTDxA_exp;
  double* DyTDyA_exp;

  double* DxA_act;
  double* DyA_act;

  double* DyTA_act;
  double* DxTA_act;

  double* DxTDxA_act;
  double* DyTDyA_act;

  double alpha;
  int N;
  int M;
  int NM;

  ~ImgDiffData() {
    l1c_free_double(A);
    l1c_free_double(DxA_exp);
    l1c_free_double(DyA_exp);
    l1c_free_double(DxTA_exp);
    l1c_free_double(DyTA_exp);
    l1c_free_double(DxTDxA_exp);
    l1c_free_double(DyTDyA_exp);

    l1c_free_double(DxA_act);
    l1c_free_double(DyA_act);
    l1c_free_double(DxTA_act);
    l1c_free_double(DyTA_act);

    l1c_free_double(DxTDxA_act);
    l1c_free_double(DyTDyA_act);
  }
};

class TotalVariationFixture : public ::testing::TestWithParam<std::string> {
public:
  void SetUp() override {
    int tmp = 0, NM = 0;

    const std::string fpath = std::string(kTestDataDir) + "/" + GetParam();
    ASSERT_EQ(0, load_file_to_json(fpath.c_str(), &test_data_json)) << fpath;

    ASSERT_EQ(0, extract_json_double_array(test_data_json, "A", &dd.A, &dd.NM));
    ASSERT_EQ(0, extract_json_double_array(test_data_json, "DxA", &dd.DxA_exp, &tmp));
    ASSERT_EQ(0, extract_json_double_array(test_data_json, "DyA", &dd.DyA_exp, &tmp));
    ASSERT_EQ(0, extract_json_double_array(test_data_json, "DxTA", &dd.DxTA_exp, &tmp));
    ASSERT_EQ(0, extract_json_double_array(test_data_json, "DyTA", &dd.DyTA_exp, &tmp));
    ASSERT_EQ(
        0, extract_json_double_array(test_data_json, "DxTDxA", &dd.DxTDxA_exp, &tmp));
    ASSERT_EQ(
        0, extract_json_double_array(test_data_json, "DyTDyA", &dd.DyTDyA_exp, &tmp));

    ASSERT_EQ(0, extract_json_double(test_data_json, "alpha", &dd.alpha));
    ASSERT_EQ(0, extract_json_int(test_data_json, "M", &dd.M));
    ASSERT_EQ(0, extract_json_int(test_data_json, "N", &dd.N));

    NM = dd.NM;

    dd.DxA_act = l1c_malloc_double(NM);
    dd.DyA_act = l1c_malloc_double(NM);
    dd.DxTA_act = l1c_malloc_double(NM);
    dd.DyTA_act = l1c_malloc_double(NM);

    dd.DxTDxA_act = l1c_malloc_double(NM);
    dd.DyTDyA_act = l1c_malloc_double(NM);
    ASSERT_NE(dd.DxA_act, nullptr);
    ASSERT_NE(dd.DyA_act, nullptr);
    ASSERT_NE(dd.DxTA_act, nullptr);
    ASSERT_NE(dd.DyTA_act, nullptr);
    ASSERT_NE(dd.DxTDxA_act, nullptr);
    ASSERT_NE(dd.DyTDyA_act, nullptr);
  }
  void TearDown() override { cJSON_Delete(test_data_json); }
  cJSON* test_data_json;
  ImgDiffData dd;
};

INSTANTIATE_TEST_SUITE_P(TvTestCases,
                         TotalVariationFixture,
                         testing::Values("TV_data_square_small.json",
                                         "TV_data_tall_skinny.json",
                                         "TV_data_short_wide.json"));

TEST_P(TotalVariationFixture, test_l1c_Dx) {
  int N = dd.N;
  int M = dd.M;

  for (int i = 0; i < N * M; i++) {
    dd.DxA_act[i] = 1;
  }

  l1c_Dx(N, M, dd.alpha, dd.A, dd.DxA_act);

  ASSERT_CVEC_NEAR(N * M, dd.DxA_exp, dd.DxA_act, kDoubleTolSuper * 10);
}

TEST_P(TotalVariationFixture, test_l1c_DxT) {

  int N = dd.N;
  int M = dd.M;

  for (int i = 0; i < N * M; i++) {
    dd.DxTA_act[i] = 1;
  }

  l1c_DxT(N, M, dd.alpha, dd.A, dd.DxTA_act);

  ASSERT_CVEC_NEAR(N * M, dd.DxTA_exp, dd.DxTA_act, kDoubleTolSuper * 10);
}

TEST_P(TotalVariationFixture, test_l1c_DxTDx) {

  int N = dd.N;
  int M = dd.M;

  for (int i = 0; i < N * M; i++) {
    dd.DxTDxA_act[i] = 1;
  }

  l1c_DxTDx(N, M, dd.alpha, dd.A, dd.DxTDxA_act);

  ASSERT_CVEC_NEAR(N * M, dd.DxTDxA_exp, dd.DxTDxA_act, kDoubleTolSuper * 100);
}

TEST_P(TotalVariationFixture, test_l1c_Dy) {
  int N = dd.N;
  int M = dd.M;

  for (int i = 0; i < N * M; i++) {
    dd.DyA_act[i] = 0;
  }

  l1c_Dy(N, M, dd.alpha, dd.A, dd.DyA_act);

  ASSERT_CVEC_NEAR(N * M, dd.DyA_exp, dd.DyA_act, kDoubleTolSuper * 10);
}

TEST_P(TotalVariationFixture, test_l1c_DyT) {
  int N = dd.N;
  int M = dd.M;

  l1c_DyT(N, M, dd.alpha, dd.A, dd.DyTA_act);

  ASSERT_CVEC_NEAR(N * M, dd.DyTA_exp, dd.DyTA_act, kDoubleTolSuper * 10);
}

TEST_P(TotalVariationFixture, test_l1c_DyTDy) {
  int N = dd.N;
  int M = dd.M;

  // double lap_y[12];
  l1c_DyTDy(N, M, dd.alpha, dd.A, dd.DyTDyA_act);

  ASSERT_CVEC_NEAR(N * M, dd.DyTDyA_exp, dd.DyTDyA_act, kDoubleTolSuper * 100);
}

} // namespace l1c
