/*
  Tests for the 1D DCT-based transforms.

 */
#include <cjson/cJSON.h>
#include <fftw3.h>
#include <gtest/gtest.h>

#include "common.h"
#include "json_utils.h"
#include "l1c.h"
#include "l1c_math.h"
#include "test_config.h"

namespace l1c {

class MatrixTransformsFixture : public ::testing::Test {
public:
  /* We the tcase_add_checked_fixture method. setup() and teardown are called by
     the associated setup and teardown functions for each test case. This allows us
     to use a different file path for each.
   */
  void SetUp() override {
    const std::string fpath = std::string(kTestDataDir) + "/matrix_xfm_small_data.json";

    ASSERT_EQ(0, load_file_to_json(fpath.c_str(), &test_data_json)) << fpath;

    ASSERT_EQ(0, extract_json_double_array(test_data_json, "A", &A, &NM));
    ASSERT_EQ(0, extract_json_double_array(test_data_json, "x", &x_in, &Mcol));
    ASSERT_EQ(0, extract_json_double_array(test_data_json, "y", &y_in, &Nrow));

    ASSERT_EQ(0, extract_json_double_array(test_data_json, "AtAx", &AtAx_exp, &Mcol));

    ASSERT_EQ(0, extract_json_double_array(test_data_json, "Aty", &Aty_exp, &Mcol));

    ASSERT_EQ(0, extract_json_double_array(test_data_json, "Ax", &Ax_exp, &Nrow));

    AtAx_act = l1c_malloc_double(Mcol);
    Aty_act = l1c_malloc_double(Mcol);
    Ax_act = l1c_malloc_double(Nrow);

    /* cblas_dgemv works like:
       y = Ax + beta*y. We had a bug where beta=0, but
       initializing the result vectors to 0 covers that up.
    */
    l1c_init_vec(Mcol, AtAx_act, 1);
    l1c_init_vec(Mcol, Aty_act, 1);
    l1c_init_vec(Nrow, Ax_act, 1);

    l1c_setup_matrix_transforms(Nrow, Mcol, A, &ax_funs);
  }

  void TearDown() override {
    cJSON_Delete(test_data_json);

    l1c_free_double(A);
    l1c_free_double(x_in);
    l1c_free_double(y_in);

    l1c_free_double(AtAx_exp);
    l1c_free_double(Aty_exp);
    l1c_free_double(Ax_exp);

    l1c_free_double(AtAx_act);
    l1c_free_double(Aty_act);
    l1c_free_double(Ax_act);

    ax_funs.destroy();
  }

  double* A;
  double* AtAx_exp;
  double* AtAx_act;
  double* Aty_act;
  double* Aty_exp;
  double* Ax_act;
  double* Ax_exp;
  double* x_in;
  double* y_in;

  int Nrow;
  int Mcol;
  int NM;

  l1c_AxFuns ax_funs;
  cJSON* test_data_json;
};

TEST_F(MatrixTransformsFixture, AtAx) {
  /* Provided and freed by setup_small() and teardown_small()*/
  ax_funs.AtAx(x_in, AtAx_act);

  ASSERT_CVEC_NEAR(Mcol, AtAx_exp, AtAx_act, kDoubleTolSuper);
}

TEST_F(MatrixTransformsFixture, Aty) {
  ax_funs.Aty(y_in, Aty_act);

  ASSERT_CVEC_NEAR(Mcol, Aty_exp, Aty_act, kDoubleTolSuper);
}

TEST_F(MatrixTransformsFixture, Ax) {
  ax_funs.Ax(x_in, Ax_act);

  ASSERT_CVEC_NEAR(Nrow, Ax_exp, Ax_act, kDoubleTolSuper);
}

} // namespace l1c
