/*
 *  Tests for the 1D DCT-based transforms.
 */
#include <cjson/cJSON.h>
#include <fftw3.h>
#include <glog/logging.h>
#include <gtest/gtest.h>

#include "common.h"
#include "json_utils.h"
#include "l1c.h"

#include "test_config.h"

namespace l1c {

struct DctData {
  DctData() = default;
  DctData(const DctData&) = delete;
  DctData(DctData&&) = delete;
  DctData operator=(DctData&&) = delete;
  DctData operator=(const DctData&) = delete;

  int* pix_idx;
  double* MtEty_EMx_exp;
  double* MtEty_EMx_act;
  double* MtEty_act;
  double* MtEty_exp;
  double* EMx_act;
  double* EMx_exp;

  double* Mx_act;
  double* Mx_exp;
  double* Mty_act;
  double* Mty_exp;

  double* Ex_act;
  double* Ex_exp;
  double* Ety_act;
  double* Ety_exp;

  double* x_in;
  double* y_in;
  double* z_in;

  int m;
  int n;

  int setup_status;
  std::string fpath;

  ~DctData() {
    l1c_free_double(x_in);
    l1c_free_double(y_in);
    l1c_free_double(z_in);

    l1c_free_double(MtEty_EMx_exp);
    l1c_free_double(MtEty_exp);
    l1c_free_double(EMx_exp);
    l1c_free_double(Mx_exp);
    l1c_free_double(Mty_exp);
    l1c_free_double(Ex_exp);
    l1c_free_double(Ety_exp);

    l1c_free_double(MtEty_EMx_act);
    l1c_free_double(MtEty_act);
    l1c_free_double(EMx_act);
    l1c_free_double(Mx_act);
    l1c_free_double(Mty_act);
    l1c_free_double(Ex_act);
    l1c_free_double(Ety_act);

    free(pix_idx);
  }
};

/* Global variable for each all test cases.  */

/* We the tcase_add_checked_fixture method. setup() and teardown are called by
   the associated setup and teardown functions for each test case. This allows us
   to use a different file path for each.
 */
class DctTests : public testing::TestWithParam<std::string> {
public:
  DctData dctd_;
  l1c_AxFuns ax_funs_;

  void SetUp() override {
    const std::string fname = GetParam();

    cJSON* test_data_json;

    dctd_.fpath = std::string(kTestDataDir) + "/" + fname;

    CHECK(0 == load_file_to_json(dctd_.fpath.c_str(), &test_data_json))
        << "Error loading data in test_dct from file: " << dctd_.fpath;

    CHECK_EQ(0,
             extract_json_double_array(test_data_json, "x_in", &dctd_.x_in, &dctd_.m));
    CHECK_EQ(0,
             extract_json_double_array(test_data_json, "y_in", &dctd_.y_in, &dctd_.n));
    CHECK_EQ(0,
             extract_json_double_array(test_data_json, "z_in", &dctd_.z_in, &dctd_.m));

    CHECK_EQ(0,
             extract_json_double_array(
                 test_data_json, "MtEt_EMx", &dctd_.MtEty_EMx_exp, &dctd_.m));

    CHECK_EQ(
        0,
        extract_json_double_array(test_data_json, "MtEty", &dctd_.MtEty_exp, &dctd_.m));

    CHECK_EQ(
        0, extract_json_double_array(test_data_json, "EMx", &dctd_.EMx_exp, &dctd_.n));

    CHECK_EQ(0,
             extract_json_double_array(test_data_json, "Mx", &dctd_.Mx_exp, &dctd_.m));
    CHECK_EQ(
        0, extract_json_double_array(test_data_json, "Mty", &dctd_.Mty_exp, &dctd_.m));
    CHECK_EQ(0,
             extract_json_double_array(test_data_json, "Ex", &dctd_.Ex_exp, &dctd_.n));
    CHECK_EQ(
        0, extract_json_double_array(test_data_json, "Ety", &dctd_.Ety_exp, &dctd_.m));

    CHECK_EQ(
        0, extract_json_int_array(test_data_json, "pix_idx", &dctd_.pix_idx, &dctd_.n));

    dctd_.MtEty_EMx_act = l1c_malloc_double(dctd_.m);
    dctd_.MtEty_act = l1c_malloc_double(dctd_.m);
    dctd_.EMx_act = l1c_malloc_double(dctd_.m);
    dctd_.Mx_act = l1c_malloc_double(dctd_.m);
    dctd_.Mty_act = l1c_malloc_double(dctd_.m);
    dctd_.Ex_act = l1c_calloc_double(dctd_.n);
    dctd_.Ety_act = l1c_malloc_double(dctd_.m);

    l1c_dct1_setup(dctd_.n, dctd_.m, dctd_.pix_idx, &ax_funs_);

    cJSON_Delete(test_data_json);
  }
  void TearDown() override { ax_funs_.destroy(); }
};

INSTANTIATE_TEST_SUITE_P(DctTestCases,
                         DctTests,
                         testing::Values("dct_small.json",
                                         "dct_large.json",
                                         "/dct_small_pure_dct.json"));

TEST_P(DctTests, TestDctMtEt_EMx) {
  ax_funs_.AtAx(dctd_.x_in, dctd_.MtEty_EMx_act);
  ASSERT_CVEC_NEAR(dctd_.m, dctd_.MtEty_EMx_exp, dctd_.MtEty_EMx_act, 0.01);
}

TEST_P(DctTests, TestDctMtEty) {
  ax_funs_.Aty(dctd_.y_in, dctd_.MtEty_act);
  ASSERT_CVEC_NEAR(dctd_.m, dctd_.MtEty_exp, dctd_.MtEty_act, kDoubleTolSuper);
}

TEST_P(DctTests, TestDctEMx) {
  ax_funs_.Ax(dctd_.x_in, dctd_.EMx_act);
  ASSERT_CVEC_NEAR(dctd_.n, dctd_.EMx_exp, dctd_.EMx_act, kDoubleTolSuper);
}

TEST_P(DctTests, TestDctMx) {
  ax_funs_.Mx(dctd_.x_in, dctd_.Mx_act);
  ASSERT_CVEC_NEAR(dctd_.m, dctd_.Mx_exp, dctd_.Mx_act, kDoubleTolSuper);
}

TEST_P(DctTests, TestDctWtx_synth) {
  /* Should be the identity.*/
  ax_funs_.Wtx(dctd_.x_in, dctd_.Mx_act);
  ASSERT_CVEC_NEAR(dctd_.m, dctd_.x_in, dctd_.Mx_act, kDoubleTolSuper);
}

TEST_P(DctTests, TestDctWz_synth) {
  /* Should be the identity.*/
  ax_funs_.Wz(dctd_.x_in, dctd_.Mx_act);
  ASSERT_CVEC_NEAR(dctd_.m, dctd_.x_in, dctd_.Mx_act, kDoubleTolSuper);
}

TEST_P(DctTests, TestDctMty) {
  ax_funs_.Mty(dctd_.z_in, dctd_.Mty_act);
  ASSERT_CVEC_NEAR(dctd_.m, dctd_.Mty_exp, dctd_.Mty_act, 2 * kDoubleTolSuper);
}

TEST_P(DctTests, TestDctRx) {
  ax_funs_.Rx(dctd_.x_in, dctd_.Ex_act);
  ASSERT_CVEC_NEAR(dctd_.n, dctd_.Ex_exp, dctd_.Ex_act, kDoubleTolSuper);
}

TEST_P(DctTests, TestDctRty) {
  ax_funs_.Rty(dctd_.y_in, dctd_.Ety_act);
  ASSERT_CVEC_NEAR(dctd_.m, dctd_.Ety_exp, dctd_.Ety_act, kDoubleTolSuper);
}

} // namespace l1c
