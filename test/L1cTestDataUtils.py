#!/usr/bin/env python3
import codecs
import json
from pathlib import Path

import numpy as np
import test_config


def jsonify(data):

    for key, value in data.items():
        if type(value) is np.ndarray:
            if len(value.flatten()) == 1:
                data[key] = value.flatten()[0]
            else:
                data[key] = value.flatten().tolist()

    return data


def save_json(data, file_path):
    file_path = Path(file_path)
    file_path.parent.mkdir(exist_ok=True)
    with codecs.open(str(file_path), "w") as fid:
        json.dump(data, fid, separators=(",", ":"), sort_keys=True, indent=4)


def data_dir():
    return test_config.TEST_DATA_DIR
