#include <gtest/gtest.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include "json_utils.h"
#include "l1c_logging.h"

namespace l1c {

static constexpr char fname_tmp[] = "test_scratch.txt";

int l1c_printf_new(const char* format, ...) {
  FILE* fid = fopen(fname_tmp, "w+");
  if (!fid) {
    return -1;
  }
  fprintf(fid, "NEW:");

  va_list args;
  va_start(args, format);
  int status = vfprintf(fid, format, args);
  va_end(args);

  fclose(fid);
  return status;
}

/* Check that we can reset the old printf */
TEST(L1cLogging, test_l1c_replace_printf) {
  char str_expected[] = "NEW:test format: 5, 3.14";
  char* str_actual;
  l1c_printf_t* old_printf = l1c_replace_printf(l1c_printf_new);

  l1c_printf("test format: %d, %.2f", 5, 3.14);
  load_file_as_text(fname_tmp, &str_actual);
  ASSERT_EQ(std::string(str_actual), std::string(str_expected));
  free(str_actual);

  /*Now, we re-open the file, write a single character,
    (which will erase the old contents) */
  FILE* fid = fopen(fname_tmp, "w+");
  ASSERT_NE(nullptr, fid);
  fprintf(fid, "1");
  fclose(fid);

  /*Now, reset l1c_printf to the old_printf (which should be the system printf)*/
  l1c_replace_printf(old_printf);
  /* Now, when we call l1c_printf, it should not go to the file.
   I dont know how to check that it goes to stdout. */
  l1c_printf("test format: %d, %.2f", 5, 3.14);
  load_file_as_text(fname_tmp, &str_actual);

  ASSERT_EQ(std::string(str_actual), "1");

  free(str_actual);
}

} // namespace l1c
