#include <gtest/gtest.h>

#include "common.h"
#include "l1c.h"

namespace l1c {

TEST(L1cMemoryTests, TestL1cMallocdouble) {
#define N_trial 50
  int N = 256 * 256;
  double* dptr_array[N_trial];
  double* dptr = NULL;
  int i;
  uintptr_t ptr_as_int;

  /* Do this a bunch of times, to try and hedge against the possibility that the
   allocated address just happens to a multiple of 64*/
  for (i = 0; i < N_trial; i++) {

    dptr = l1c_malloc_double(N);
    ptr_as_int = (uintptr_t)dptr;
    ASSERT_EQ(0, (void*)(ptr_as_int & (DALIGN - 1)));

    dptr_array[i] = dptr;
  }

  for (i = 0; i < N_trial; i++) {
    l1c_free_double(dptr_array[i]);
  }
}

TEST(L1cMemoryTests, TestL1cMallocDouble2D) {
#define N_trial 50
  int mcol = 256 * 256;
  int nrow = 3;
  double** dptr_2D = NULL;
  uintptr_t ptr_as_int;

  /* Do this a bunch of times, to try and hedge against the possibility that the
     allocated address just happens to a multiple of 64*/
  for (int i = 0; i < N_trial; i++) {
    dptr_2D = l1c_malloc_double_2D(nrow, mcol);

    for (int k = 0; k < nrow; k++) {
      ptr_as_int = (uintptr_t)dptr_2D[k];
      ASSERT_EQ(0, (void*)(ptr_as_int & (DALIGN - 1)));
    }

    l1c_free_double_2D(nrow, dptr_2D);
  }
}

TEST(L1cMemoryTests, TestL1CcallocDouble2D) {
  int N = 5;
  double x_exp[5] = {0, 0, 0, 0, 0};
  double** dptr = NULL;

  dptr = l1c_calloc_double_2D(2, N);

  for (int i = 0; i < 2; i++) {
    ASSERT_CVEC_NEAR(N, x_exp, dptr[i], 1e-15);
  }
  l1c_free_double_2D(2, dptr);
}

TEST(L1cMemoryTests, TestL1cFreeDouble2DWhenNull) {
  double** dptr_2D = NULL;

  /* Nothing should happen. If it enters the loop, we should get segfault,
     and the test fails.
  */
  l1c_free_double_2D(2, dptr_2D);

  dptr_2D = l1c_malloc_double_2D(2, 10);

  /* I dont know what to do here, I guess just let the valgrind check
     complain if this didnt work right.
  */
  l1c_free_double_2D(2, dptr_2D);
}

TEST(L1cMemoryTests, TestL1cCallocDouble) {
  int N = 5;
  double x_exp[5] = {0, 0, 0, 0, 0};
  double* dptr = NULL;

  dptr = l1c_calloc_double(N);

  ASSERT_CVEC_NEAR(N, x_exp, dptr, 1e-15);

  l1c_free_double(dptr);
}

} // namespace l1c
