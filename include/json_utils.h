#pragma once
#ifdef __cplusplus
extern "C" {
#endif
#include <stdio.h>

#include <cjson/cJSON.h>

#include "l1c.h"

long get_file_length(FILE* file_ptr);

int extract_json_double_array(cJSON* data_json,
                              const char* name,
                              double** x,
                              l1c_int* N);

int load_file_as_text(const char* fname, char** file_data);

int load_file_to_json(const char* fname, cJSON** data_json);

void print_vec(l1c_int N, double* x, const char* name);

int extract_json_int(cJSON* data_json, const char* name, l1c_int* N);

int extract_json_double(cJSON* data_json, const char* name, double* a);

int extract_json_int_array(cJSON* data_json, const char* name, l1c_int** x, l1c_int* N);

#ifdef __cplusplus
} // __cplusplus
#endif
