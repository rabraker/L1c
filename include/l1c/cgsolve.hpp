#pragma once

#include "l1c.h"
/**
 * Struct containing artifacts of the cgsolve routine.
 */
typedef struct _l1c_CgResults {
  /** Residual */
  double cgres;
  /** Number of completed conjugate gradient iterations. */
  int cgiter;

} l1c_CgResults;

/**
 * Parameters for the conjugate gradient solver.
 */
typedef struct _l1c_CgParams {
  /** If 0, print nothing, if >0, print status every verbose-th iteration. */
  l1c_int verbose;
  /** Maximum number of solver iterations.*/
  l1c_int max_iter;
  /** Solver tolerance.*/
  double tol;
} l1c_CgParams;

#ifdef __cplusplus
extern "C" {
namespace l1c {
#endif

/** @}*/

/**
 * @ingroup lin_solve
 *
 * Conjugate gradient solver.
 *
 * Purpose
 * -------
 * To solve the system of equations
 * \f[
 * A x = b
 * \f]
 * where \f$A = A^T > 0\f$ via the method of conjugate gradients.
 * The advantage to this method is that we do not have to
 * store the matrix A, but only need a function which performs
 * the linear mapping.
 *
 * Algorithm
 * ---------
 * This code is a c implementation of the conjugate gradient solver
 * in l1magic. It closely follows the wikipedia description
 *
 * https://en.wikipedia.org/wiki/Conjugate_gradient_method
 *
 * @param[in] N Length of the vector b.
 * @param[out] x The result is stored in this array. Should have length N.
 * @param[in] b  The RHS, should have length N.
 * @param[in] Dwork array of Pointers to 4 work arrays of length N.
 * @param[in] AX_func Pointer to a function which evalutes A * x.
 * @param[in] AX_data  Data needed by AX_func. For example, if
 * you just wanted AX_func to perform a normal matrix multiplication,
 * you could do
 * AX_func((int n, double *x, double *b, void *AX_data){
 * double *A = (double *) AX_data;
 * @param[out] cg_result Pointer to a struct containing relevent results from the
 * computation.
 * @param[in] cg_params Struct containing parameters (tolerance and verbosity) for the
 * computation.
 *
 */
int cgsolve(l1c_int N,
            double* x_,
            double* b_,
            double** Dwork_,
            void (*AX_func)(l1c_int n, double* x, double* b, void* AX_data),
            void* AX_data,
            l1c_CgResults* cg_result,
            l1c_CgParams cg_params);

#ifdef __cplusplus
} // namespace l1c
} // extern "C"
#endif
