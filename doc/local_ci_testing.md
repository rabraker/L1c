# Running CI locally

Basically, install gitlab-runner and docker.

see here:

https://cylab.be/blog/30/running-gitlab-tests-locally-with-docker-ce

Then,

```
gitlab-runner exec docker main
```
or, if using a locally build docker image

```
gitlab-runner exec docker --docker-pull-policy=never main
```
**IMPORTANT** You must commit changes for them to show up inside the build.
The only exceptions *seems* to be the `.gitlab-ci.yaml` file itself.


See [docs here](https://docs.gitlab.com/runner/executors/docker.html#how-pull-policies-work)

# Building and Pushing a new image
The docker image is cached, which drastically improves CI time. To build and push a 
new image:

```
cd tools/
docker login registry.gitlab.com
docker build -t registry.gitlab.com/rabraker/l1c .
docker push registry.gitlab.com/rabraker/l1cc
```
The registry is https://gitlab.com/rabraker/ctrlc/container_registry.
