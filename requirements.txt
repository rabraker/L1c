black==22.10.0
conan==2.0.14
isort==5.10.1
numpy==1.23
scipy==1.9.3
matplotlib==3.6.2
