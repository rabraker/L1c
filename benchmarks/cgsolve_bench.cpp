/*
 * Bencharks for the conjugate gradient solver. These just the tests
 * converted to a benchmark.
 */
#include <benchmark/benchmark.h>

#include "cblas.h"
#include <cjson/cJSON.h>
#include <glog/logging.h>
#include <stdlib.h>

#include "json_utils.h"
#include "l1c.h"
#include "l1c/cgsolve.hpp"
#include "l1c_math.h"
#include "l1qc_newton.h"
#include "test_config.h"

namespace l1c {

/*
   For test routines.
   Computes the matrix-vector product y = A * b, for a symmetric matrix A.
   This is a wrapper for cblas_dspmv.
*/
static void Ax_sym(int n, double* x, double* b, void* AX_data) {

  double* A = (double*)AX_data;

  cblas_dspmv(CblasRowMajor, CblasUpper, n, 1.0, A, x, 1, 0.0, b, 1);
}

class CgsolveSmallFixture : public ::benchmark::Fixture {
public:
  CgsolveSmallFixture() { Iterations(100000); }
  void SetUp(::benchmark::State& /*state */) override {
    int Nx = 0, Nb = 0;
    const std::string fpath = std::string(kTestDataDir) + "/cgsolve_small01.json";
    CHECK_EQ(0, load_file_to_json(fpath.c_str(), &test_data_json)) << fpath;

    CHECK_EQ(0, extract_json_int(test_data_json, "max_iter", &max_iter));
    CHECK_EQ(0, extract_json_double(test_data_json, "tol", &tol));

    CHECK_EQ(0, extract_json_double_array(test_data_json, "x", &x_exp, &Nx));
    CHECK_EQ(0, extract_json_double_array(test_data_json, "b", &b, &Nb));
    CHECK_EQ(0, extract_json_double_array(test_data_json, "A", &A, &na));

    N = Nx;

    x = l1c_malloc_double(N);
    CHECK_NE(x, nullptr);
    dwork = l1c_malloc_double_2D(kNumDwork, N);
    CHECK_NE(dwork, nullptr);
    CHECK_NE(x, nullptr);

    CHECK_EQ(Nx, Nb);
    CHECK_EQ((int)(Nb * (Nb + 1) / 2), na);
  }

  void TearDown(::benchmark::State& /*state*/) override {
    l1c_free_double(A);
    l1c_free_double(x);
    l1c_free_double(x_exp);
    l1c_free_double(b);
    l1c_free_double_2D(kNumDwork, dwork);
    cJSON_Delete(test_data_json);
  }

  double* A;
  double* x;
  double* b;
  double** dwork;
  double* x_exp;
  int N;
  int na;
  int max_iter;
  double tol;
  static constexpr size_t kNumDwork = 5;
  cJSON* test_data_json;
};

BENCHMARK_F(CgsolveSmallFixture, TestCgsolveEigen)(benchmark::State& st) {
  l1c_CgParams cgp;
  l1c_CgResults cgr;

  cgp.verbose = 0;
  cgp.tol = tol;
  cgp.max_iter = max_iter;

  /* Must initialize x now, with warm starting. */
  for (int i = 0; i < N; i++) {
    x[i] = 0.0;
  }

  for (auto _ : st) {
    cgsolve(N, x, b, dwork, Ax_sym, A, &cgr, cgp);
  }
}

class CgsolveH11pFixture : public ::benchmark::Fixture {
public:
  void SetUp(::benchmark::State& /*st */) override {
    std::string fpath = std::string(kTestDataDir) + "/l1qc_data.json";

    CHECK_EQ(0, load_file_to_json(fpath.c_str(), &test_data_json)) << fpath;

    // Inputs to get_gradient
    CHECK_EQ(0, extract_json_double_array(test_data_json, "atr", &atr, &m));
    CHECK_EQ(0, extract_json_double_array(test_data_json, "sigx", &sigx, &m));
    CHECK_EQ(0, extract_json_double_array(test_data_json, "w1p", &w1p, &m));
    CHECK_EQ(0, extract_json_double_array(test_data_json, "dx", &dx_exp, &m));
    // status +=extract_json_double_array(test_data_json, "dx0", &dx0, &m);

    CHECK_EQ(0, extract_json_int_array(test_data_json, "pix_idx", &pix_idx, &n));

    CHECK_EQ(0, extract_json_double(test_data_json, "fe", &fe));
    CHECK_EQ(0, extract_json_double(test_data_json, "cgtol", &cgtol));
    CHECK_EQ(0, extract_json_double(test_data_json, "tau", &tau));
    CHECK_EQ(0, extract_json_int(test_data_json, "cgmaxiter", &cg_maxiter));

    l1c_dct1_setup(n, m, pix_idx, &ax_funs);

    h11p_data.one_by_fe = 1.0 / fe;
    h11p_data.one_by_fe_sqrd = 1.0 / (fe * fe);
    h11p_data.atr = atr;
    h11p_data.sigx = sigx;
    h11p_data.Dwork_1m = l1c_malloc_double(m);
    h11p_data.AtAx = ax_funs.AtAx;

    DWORK4 = l1c_malloc_double_2D(4, m);
    dx0 = l1c_malloc_double(m);
    l1c_init_vec(m, dx0, 0);
    dx_by_nrm = l1c_malloc_double(4 * m);
    dx_by_nrm_exp = l1c_malloc_double(4 * m);
    CHECK_NOTNULL(DWORK4);
    CHECK_NOTNULL(dx_by_nrm);
    CHECK_NOTNULL(dx_by_nrm_exp);

    cgp.max_iter = cg_maxiter;
    cgp.tol = cgtol;
    cgp.verbose = 0;
  }
  void TearDown(benchmark::State&) override {
    l1c_free_double(dx_by_nrm_exp);
    l1c_free_double(dx_by_nrm);
    l1c_free_double(atr);
    l1c_free_double(sigx);
    l1c_free_double(w1p);
    l1c_free_double(dx_exp);
    free(pix_idx);
    l1c_free_double(dx0);
    l1c_free_double_2D(4, DWORK4);
    l1c_free_double(h11p_data.Dwork_1m);
    ax_funs.destroy();

    cJSON_Delete(test_data_json);
  }
  cJSON* test_data_json;

  Hess_data h11p_data;
  double* atr;
  double* sigx;
  double* dx0;
  double* dx_exp;
  double* dx_by_nrm;
  double* dx_by_nrm_exp;
  double* w1p;
  double** DWORK4;
  double fe;
  double cgtol;
  double tau = 0;
  l1c_CgResults cgr;
  l1c_CgParams cgp = {.verbose = 0, .max_iter = 0, .tol = 0};

  int m;
  int n;
  int cg_maxiter;
  int* pix_idx;
  l1c_AxFuns ax_funs;
};

BENCHMARK_F(CgsolveH11pFixture, TestCgsolveH11pEigen)(benchmark::State& st) {
  for (auto _ : st) {
    cgsolve(m, dx0, w1p, DWORK4, _l1c_l1qc_H11pfun, &h11p_data, &cgr, cgp);
  }
}

} // namespace l1c

BENCHMARK_MAIN();
