#include <stdio.h>
#include <stdlib.h>
#include <string>

#include <cjson/cJSON.h>
#include <glog/logging.h>

#include "json_utils.h"
#include "l1c.h"

struct Data {
  Data() = default;
  Data(const Data&) = delete;
  Data(Data&&) = delete;
  Data operator=(const Data&) = delete;
  Data operator=(Data&&) = delete;

  ~Data() {
    l1c_free_double(x);
    free(pix_idx);
    l1c_free_double(b);
    cJSON_Delete(test_data_json);
  }
  cJSON* test_data_json;
  double* x;
  double* b;
  int* pix_idx;
  int m = 0;
  int n = 0;
  int status = 0;
};

int main(int argc, char** argv) {
  if (argc < 3) {
    printf("Two arguments are required.");
  }
  const int verbose = atoi(argv[2]);
  const std::string fpath(argv[1]);

  l1c_LBResult lb_res;
  l1c_L1qcOpts l1qc_opts = {.epsilon = .1,
                            .mu = 10,
                            .lbtol = 1e-3,
                            .tau = 0.0,
                            .lbiter = 0,
                            .newton_tol = 1e-3,
                            .newton_max_iter = 50,
                            .verbose = verbose,
                            .l1_tol = 1e-5,
                            .cg_tol = 1e-8,
                            .cg_maxiter = 200,
                            .cg_verbose = 0,
                            .warm_start_cg = 0,
                            .dct_mode = dct1};
  Data data{};
  CHECK_EQ(0, load_file_to_json(fpath.c_str(), &data.test_data_json))
      << "Error loading data in l1qc_dct_c";

  CHECK_EQ(0, extract_json_double_array(data.test_data_json, "b", &data.b, &data.n));
  CHECK_EQ(
      0,
      extract_json_int_array(data.test_data_json, "pix_idx", &data.pix_idx, &data.n));
  CHECK_EQ(0, extract_json_int(data.test_data_json, "mtot", &data.m));

  data.x = l1c_malloc_double(data.m);
  CHECK_NOTNULL(data.x);

  l1qc_dct(data.m, 1, data.x, data.n, data.b, data.pix_idx, l1qc_opts, &lb_res);

  return 0;
}
