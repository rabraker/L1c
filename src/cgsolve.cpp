/**  @file
This contains the conjugate gradient solver, cgsolve. The two small routines Ax and
Ax_sym illustrate how the user function AX_func can parse the input void *AX_data.

*/

#include <math.h>

#include <Eigen/Core>

#include "l1c.h"
#include "l1c/cgsolve.hpp"
#include "l1c_logging.h"
#include "l1c_math.h"

extern "C" {
namespace l1c {

static void cg_report(int iter,
                      double best_rel_res,
                      double rel_res,
                      double alpha,
                      double beta,
                      double delta);

int cgsolve(l1c_int N,
            double* x_,
            double* b_,
            double** Dwork_,
            void (*AX_func)(l1c_int n, double* x, double* b, void* AX_data),
            void* AX_data,
            l1c_CgResults* cg_result,
            l1c_CgParams cg_params) {

  int iter;

  double rel_res = 0.0;
  double beta = 0.0;

  Eigen::Map<Eigen::VectorXd> x(x_, N, 1);
  Eigen::Map<Eigen::VectorXd> b(b_, N, 1);

  /* Divide up Dwork for tempory variables */
  Eigen::Map<Eigen::VectorXd> r(Dwork_[0], N, 1);
  Eigen::Map<Eigen::VectorXd> p(Dwork_[1], N, 1);
  Eigen::Map<Eigen::VectorXd> bestx(Dwork_[2], N, 1);
  Eigen::Map<Eigen::VectorXd> q(Dwork_[3], N, 1);

  /* Init
  x = zeros(n,1)
  r = b-A*x
  p = r;
  delta = r'*r;
  delta_0 = b'*b;
  numiter = 0;
  bestx = x;
  bestres = sqrt(delta/delta_0);
  */
  bestx = x;

  // OLD: cblas_dcopy((int)N, b, 1, r, 1);       /*r=b: copy b (ie, z_i_1) to r */
  /*Using warmstart: set r = b - A*x  */
  AX_func(N, x.data(), r.data(), AX_data); /* r = A * x                   */
  r = b - r;
  p = r;

  double delta = r.squaredNorm();

  double delta_0 = b.squaredNorm();
  double best_rel_res = sqrt(delta / delta_0);

  for (iter = 1; iter <= cg_params.max_iter; iter++) {
    AX_func(N, p.data(), q.data(), AX_data); /* q = A * p */

    double alpha = delta / p.dot(q);

    x = alpha * p + x;
    r = r - alpha * q;

    double delta_old = delta;
    delta = r.squaredNorm();

    beta = delta / delta_old;
    p = r + beta * p;

    rel_res = sqrt(delta / delta_0);
    if (rel_res < best_rel_res) {
      bestx = x;
      best_rel_res = rel_res;
    }

    // modulo 0 is a floating point exception.
    if (cg_params.verbose > 0 && (iter % cg_params.verbose) == 0) {
      cg_report(iter, best_rel_res, rel_res, alpha, beta, delta);
    }

    if (rel_res < cg_params.tol) {
      break;
    }
  }

  x = bestx;

  cg_result->cgres = best_rel_res;
  cg_result->cgiter = min(iter, cg_params.max_iter); // Loops increment before exiting.

  return 0;
}

/*
  Report progress of cgsolve.
 */
static void cg_report(int iter,
                      double best_rel_res,
                      double rel_res,
                      double alpha,
                      double beta,
                      double delta) {

  if (iter == 1) {
    l1c_printf("cg: |Iter| Best resid | Current resid| alpha | beta   |   delta  |\n");
  }
  l1c_printf("  %d,   %.16e, %.16e, %.16e, %.16e, %.16e  \n",
             iter,
             best_rel_res,
             rel_res,
             alpha,
             beta,
             delta);
}

} // namespace l1c
} // extern
